/*
 * eeSettings.h
 *
 *  Created on: 8 ���. 2016 �.
 *      Author: Kreyl
 */

#pragma once

#include "inttypes.h"
#include "uart.h"

// EEAddresses
#define EE_ADDR_SETTINGS        0

// Settings defaults
#define ID_MIN                  1
#define ID_MAX                  99
#define ID_DEFAULT              ID_MIN

enum DeviceType_t { dtTank=0, dtTarget=1, dtTurret=2 };

class Settings_t {
private:
    uint8_t IWriteCommon(uint32_t Offset, uint8_t Data);
public:
    // Eternal
    uint8_t ID;             // 0
    // App specific
    union {
        uint8_t _dummy01;
        DeviceType_t Type;  // 1
    };
    uint8_t Group;          // 2
    uint8_t IRPower;        // 3
    uint8_t IRDamage;       // 4
    uint8_t HitCount;       // 5
    uint8_t ShotCount;      // 6
    uint8_t ReloadTime;     // 7
    uint8_t RepairTime;     // 8
    uint8_t State;          // 9
    // ==== Methods ====
    void Init();
    void Print() {
        Uart.Printf("Settings: ID%u; Type%u; Grp%u; IrPwr%u; IrDmg%u; Hits%u; Shots%u; RelT%u; RepT%u\r\n",
                ID, Type, Group,
                IRPower, IRDamage,
                HitCount, ShotCount,
                ReloadTime, RepairTime);
    }
    uint8_t Read();
    uint8_t SetID(uint8_t NewID)             { ID = NewID; return IWriteCommon(0, NewID); }
    uint8_t SetType(DeviceType_t NewType)    { Type = NewType; return IWriteCommon(1, (uint8_t)NewType); }
    uint8_t SetGroup(uint8_t NewGroup)       { Group = NewGroup; return IWriteCommon(2, NewGroup); }
    uint8_t SetIRPower(uint8_t NewIRPower)   { IRPower = NewIRPower; return IWriteCommon(3, NewIRPower); }
    uint8_t SetIRDamage(uint8_t NewIRDamage) { IRDamage = NewIRDamage; return IWriteCommon(4, NewIRDamage); }
    uint8_t SetHitCount(uint8_t NewHitCount) { HitCount = NewHitCount; return IWriteCommon(5, NewHitCount); }
    uint8_t SetShotCount(uint8_t NewShotCount) { ShotCount = NewShotCount; return IWriteCommon(6, ShotCount); }
    uint8_t SetReloadTime(uint8_t NewReloadTime) { ReloadTime = NewReloadTime; return IWriteCommon(7, NewReloadTime); }
    uint8_t SetRepairTime(uint8_t NewRepairTime) { RepairTime = NewRepairTime; return IWriteCommon(8, NewRepairTime); }
    uint8_t SetState(uint8_t NewState)       { State = NewState; return IWriteCommon(9, State); }

    uint8_t SetAllExceptID(uint8_t *ptr);
};

#define SETTINGS_SZ     sizeof(Settings_t)

extern Settings_t Settings;
