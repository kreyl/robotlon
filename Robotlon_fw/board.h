/*
 * board.h
 *
 *  Created on: 12 ����. 2015 �.
 *      Author: Kreyl
 */

#pragma once

#include <inttypes.h>

// ==== General ====
#define BOARD_NAME          "Robotlon01"

// MCU type as defined in the ST header.
#define STM32L476xx

// Freq of external crystal if any. Leave it here even if not used.
#define CRYSTAL_FREQ_HZ         12000000

// OS timer settings
#define STM32_ST_IRQ_PRIORITY   2
#define STM32_ST_USE_TIMER      5
#define SYS_TIM_CLK             (Clk.APB1FreqHz)    // Timer 5 is clocked by APB1

//  Periphery
#define I2C1_ENABLED            FALSE
#define I2C2_ENABLED            FALSE
#define I2C3_ENABLED            TRUE
#define SIMPLESENSORS_ENABLED   TRUE

#define ADC_REQUIRED            FALSE
#define STM32_DMA_REQUIRED      TRUE    // Leave this macro name for OS

#if 1 // ========================== GPIO =======================================
// UART
#define UART_GPIO       GPIOA
#define UART_TX_PIN     2
#define UART_RX_PIN     3
#define UART_AF         AF7 // for all USARTs

// RGB LED
#define LED_RED_CH      { GPIOB, 3,  TIM2, 2, invNotInverted, omPushPull, 255 }
#define LED_GREEN_CH    { GPIOB, 10, TIM2, 3, invNotInverted, omPushPull, 255 }
#define LED_BLUE_CH     { GPIOB, 11, TIM2, 4, invNotInverted, omPushPull, 255 }
// Red LED
#define LED_RED_SINGLE  { GPIOA, 15, TIM2, 1, invNotInverted, omPushPull, 255 }
// Yellow LEDs
#define LED_1           { GPIOC, 6, TIM3, 1, invNotInverted, omPushPull, 255 }
#define LED_2           { GPIOC, 7, TIM3, 2, invNotInverted, omPushPull, 255 }
#define LED_3           { GPIOC, 8, TIM3, 3, invNotInverted, omPushPull, 255 }
#define LED_4           { GPIOC, 9, TIM3, 4, invNotInverted, omPushPull, 255 }

// IR LED
#define LED_IR          GPIOA, 4     // DAC
// IR Receiver
#define IR_RCVR_PIN     GPIOB, 7, omOpenDrain, pudNone, AF2

// Outputs
#define OUT1_PIN        GPIOC, 10, omPushPull
#define OUT2_PIN        GPIOC, 11, omPushPull
#define OUT3_PIN        GPIOC, 12, omPushPull
// Input
#define IN1_PIN         {GPIOC, 2, pudPullUp}

// Beeper
#define BEEPER_TOP      22
#define BEEPER_PIN      { GPIOB, 9, TIM17, 1, invNotInverted, omPushPull, BEEPER_TOP }

// I2C
#define I2C1_GPIO       GPIOB
#define I2C1_SCL        6
#define I2C1_SDA        7
#define I2C2_GPIO       GPIOB
#define I2C2_SCL        10
#define I2C2_SDA        11
#define I2C3_GPIO       GPIOC
#define I2C3_SCL        0
#define I2C3_SDA        1
// I2C Alternate Function
#define I2C_AF          AF4

// Radio
#define CC_GPIO         GPIOA
#define CC_GDO2         NC
#define CC_GDO0         0
#define CC_SCK          5
#define CC_MISO         6
#define CC_MOSI         7
#define CC_CS           1
// Input pin
#define CC_GDO0_IRQ     { CC_GPIO, CC_GDO0, pudNone }

#endif // GPIO

#if 1 // ========================= Timer =======================================
// IR LED
#define TMR_DAC_CHUNK               TIM6
#define TMR_DAC_SMPL                TIM7
#define TMR_DAC_CHUNK_IRQ           TIM6_DAC_IRQn
#define TMR_DAC_CHUNK_IRQ_HANDLER   Vector118
// IR Receiver
#define TMR_IR_RX                   TIM4
#endif // Timer

#if 1 // =========================== SPI =======================================
#define CC_SPI          SPI1
#define CC_SPI_AF       AF5
#endif

#if 1 // ========================== USART ======================================
#define PRINTF_FLOAT_EN FALSE
#define UART            USART2
#define UART_TX_REG     UART->TDR
#define UART_RX_REG     UART->RDR
#endif

#if ADC_REQUIRED // ======================= Inner ADC ==========================
// Clock divider: clock is generated from the APB2
#define ADC_CLK_DIVIDER		adcDiv2

// ADC channels
#define ADC_BATTERY_CHNL 	14
// ADC_VREFINT_CHNL
#define ADC_CHANNELS        { ADC_BATTERY_CHNL, ADC_VREFINT_CHNL }
#define ADC_CHANNEL_CNT     2   // Do not use countof(AdcChannels) as preprocessor does not know what is countof => cannot check
#define ADC_SAMPLE_TIME     ast24d5Cycles
#define ADC_OVERSAMPLING_RATIO  64   // 1 (no oversampling), 2, 4, 8, 16, 32, 64, 128, 256
#endif

#if 1 // =========================== DMA =======================================
// ==== Uart ====
// Remap is made automatically if required
#define UART_DMA_TX     STM32_DMA1_STREAM7
#define UART_DMA_RX     STM32_DMA1_STREAM6
#define UART_DMA_CHNL   2

// === IR TX DAC ====
#define DAC_DMA         STM32_DMA2_STREAM4
#define DAC_DMA_CHNL    3

// ==== IR RX TIM ====
#define IR_RX_TIM_DMA   STM32_DMA1_STREAM1  // TIM4 CH1 (see ir.cpp why not CH2)
#define IR_RX_TIM_DMA_CHNL  6

// ==== I2C ====
#define I2C1_DMA_TX     STM32_DMA2_STREAM7
#define I2C1_DMA_RX     STM32_DMA2_STREAM6
#define I2C1_DMA_CHNL   5
#define I2C2_DMA_TX     STM32_DMA1_STREAM4
#define I2C2_DMA_RX     STM32_DMA1_STREAM5
#define I2C2_DMA_CHNL   3
#define I2C3_DMA_TX     STM32_DMA1_STREAM2
#define I2C3_DMA_RX     STM32_DMA1_STREAM3
#define I2C3_DMA_CHNL   3

#if ADC_REQUIRED
#define ADC_DMA         STM32_DMA1_STREAM1
#define ADC_DMA_MODE    STM32_DMA_CR_CHSEL(0) |   /* DMA1 Stream1 Channel0 */ \
                        DMA_PRIORITY_LOW | \
                        STM32_DMA_CR_MSIZE_HWORD | \
                        STM32_DMA_CR_PSIZE_HWORD | \
                        STM32_DMA_CR_MINC |       /* Memory pointer increase */ \
                        STM32_DMA_CR_DIR_P2M |    /* Direction is peripheral to memory */ \
                        STM32_DMA_CR_TCIE         /* Enable Transmission Complete IRQ */
#endif // ADC

#endif // DMA
