/*
 * evt_mask.h
 *
 *  Created on: Apr 12, 2013
 *      Author: g.kruglov
 */

#pragma once

// Event masks
#define EVT_UART_NEW_CMD        EVENT_MASK(1)
#define EVT_ADC_DONE            EVENT_MASK(2)

#define EVT_LEDSEQEND_DMGD      EVENT_MASK(3)
#define EVT_LEDSEQEND_DSTRYD    EVENT_MASK(4)
#define EVT_LEDSEQEND_FIRE      EVENT_MASK(5)

#define EVT_INPUT               EVENT_MASK(8)
#define EVT_IR_RX               EVENT_MASK(9)

#define EVT_USB_DATA_OUT        EVENT_MASK(10)
#define EVT_USB_READY           EVENT_MASK(11)
#define EVT_USB_SUSPEND         EVENT_MASK(12)

#define EVT_NEW_PARAM           EVENT_MASK(22)

#define EVT_EVERY_SECOND        EVENT_MASK(31)
