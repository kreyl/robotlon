/*
 * main.cpp
 *
 *  Created on: 20 ����. 2014 �.
 *      Author: g.kruglov
 */

#include "main.h"
#include "hal.h"
#include "kl_i2c.h"
#include "radio_lvl1.h"
#include "Sequences.h"
#include "Settings.h"
#include "SimpleSensors.h"
#include "ir.h"

App_t App;
Settings_t Settings;

LedRGB_t LedRGB { LED_RED_CH, LED_GREEN_CH, LED_BLUE_CH };
LedSmooth_t LedFire (LED_RED_SINGLE);
LedSmooth_t LedY1 (LED_2);
LedSmooth_t LedY2 (LED_3);
LedSmooth_t LedY3 (LED_1);
LedSmooth_t LedY4 (LED_4);
LedSmooth_t &LedR = LedY3;
LedSmooth_t &LedG = LedY4;

irLed_t irLed;

// Outputs
PinOutput_t Out1 {OUT1_PIN};
PinOutput_t Out2 {OUT2_PIN};
PinOutput_t Out3 {OUT3_PIN};

static TmrKL_t TmrEverySecond {MS2ST(1000), EVT_EVERY_SECOND, tktPeriodic};

DevInfo_t DevInfo;
void OnRadioRx();

CircBufNumber_t<Convert::WordBytes_t, 7> ParamBuf;

#if 1 // ======================== Device classes ===============================
enum DevState_t {dstPrepare = 0, dstOperational = 1, dstDamaged = 2, dstReload = 3, dstDestroyed = 4, dstStop = 5};

class DevTmr_t {
private:
    uint32_t Counter = 0;
public:
    void StartOrRestart(uint32_t Delay_s) { Counter = Delay_s; }
    void Stop() { Counter = 0; }
    void EvtNewSecond() { if(Counter > 0) Counter--; }
    bool IsEnded() { return (Counter == 0); }
};

class Device_t {
private:
    DevTmr_t TmrReload, TmrRepair;
    void ResetStateChangers() {
        TmrReload.Stop();
        TmrRepair.Stop();
        LedRGB.SetupSeqEndEvt(nullptr); // Disable evt
        LedR.SetupSeqEndEvt(nullptr); // Disable evt
    }
public:
    irPkt_t irPktTx;
    void SetState(DevState_t NewState) {
        DevInfo.State = NewState;
        Settings.SetState((uint8_t)NewState);   // Save state
        switch(NewState) {
            case dstPrepare:
                Uart.Printf("Prepare\r\n");
                ResetStateChangers();
                // Outputs
                Out1.SetLo();
                Out2.SetHi();
                Out3.SetHi();
                // === Setup indication ===
                LedRGB.StartOrRestart(lsqPrepare);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedY3.StartOrRestart(lsqOff);
                    LedY4.StartOrRestart(lsqOff);
                }
                else {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedR.StartOrRestart(lsqOff);
                    LedG.StartOrRestart(lsqSmoothPrepare);
                }
                break;

            case dstOperational:
                Uart.Printf("Operational\r\n");
                ResetStateChangers();
                // Outputs
                Out1.SetLo();
                Out2.SetLo();
                if(DevInfo.ShotCnt != 0) Out3.SetLo();
                else Out3.SetHi();
                // Fire now if device type is turret
                if(Settings.Type == dtTurret) App.SignalEvt(EVT_INPUT);
                // === Setup indication ===
                LedRGB.StartOrRestart(lsqOperational);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedY3.StartOrRestart(lsqOff);
                    LedY4.StartOrRestart(lsqOff);
                }
                else {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedR.StartOrRestart(lsqOff);
                    LedG.StartOrRestart(lsqSteady);
                }
                break;

            case dstDamaged:
                Uart.Printf("Damaged\r\n");
                Out1.SetLo();
                Out2.SetHi();
                Out3.SetHi();
                // === Setup indication ===
                LedRGB.StartOrContinue(lsqDamaged);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqSmoothDamaged);
                    LedY2.StartOrRestart(lsqSmoothDamaged);
                    LedY3.StartOrRestart(lsqSmoothDamaged);
                    LedY4.StartOrRestart(lsqSmoothDamaged);
                }
                else {
                    LedY1.StartOrRestart(lsqSmoothDamaged);
                    LedY2.StartOrRestart(lsqSmoothDamaged);
                    LedR.StartOrRestart(lsqOff);
                    LedG.StartOrRestart(lsqOff);
                }
                break;

            case dstReload:
                Uart.Printf("Reload\r\n");
                Out1.SetLo();
                Out2.SetLo();
                Out3.SetHi();
                // === Setup indication ===
                LedRGB.StartOrContinue(lsqReload);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedY3.StartOrRestart(lsqOff);
                    LedY4.StartOrRestart(lsqOff);
                }
                else {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedR.StartOrRestart(lsqOff);
                    LedG.StartOrContinue(lsqSmoothReload);
                }
                break;

            case dstDestroyed:
                Uart.Printf("Destroyed\r\n");
                ResetStateChangers();
                // Outputs
                Out1.SetHi();
                Out2.SetLo();
                Out3.SetHi();
                // === Setup indication ===
                LedRGB.StartOrRestart(lsqDestroyed);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqOff);
                    LedY2.StartOrRestart(lsqOff);
                    LedY3.StartOrRestart(lsqOff);
                    LedY4.StartOrRestart(lsqOff);
                }
                else {
                    LedY1.StartOrRestart(lsqSteadyLow);
                    LedY2.StartOrRestart(lsqSteadyLow);
                    LedR.StartOrRestart(lsqSteady);
                    LedG.StartOrContinue(lsqOff);
                }
                break;

            case dstStop:
                Uart.Printf("Stop\r\n");
                ResetStateChangers();
                // Outputs
                Out1.SetHi();
                Out2.SetLo();
                Out3.SetHi();
                break;
        } // switch state
    } // SetState

    void EvtHandler(uint32_t Evt) {
        if(Evt & EVT_LEDSEQEND_DMGD) {
            LedRGB.SetupSeqEndEvt(nullptr); // Disable evt
            SetState(dstDamaged);
        }
        if(Evt & EVT_LEDSEQEND_DSTRYD) {
            LedRGB.SetupSeqEndEvt(nullptr); // Disable evt
            SetState(dstDestroyed);
        }
        if(Evt & EVT_LEDSEQEND_FIRE) {
            LedR.SetupSeqEndEvt(nullptr); // Disable evt
            if(DevInfo.ShotCnt == 0) SetState(dstOperational);
            else SetState(dstReload);
        }

        if(Evt & EVT_EVERY_SECOND) {
            DevState_t DevSt = (DevState_t)DevInfo.State;
            if(ANY_OF_3(DevSt, dstOperational, dstReload, dstDamaged)) {
//                Uart.Printf("1s\r");
                TmrReload.EvtNewSecond();
                TmrRepair.EvtNewSecond();
                // Check if reload ended
                if(DevSt == dstReload and TmrReload.IsEnded()) SetState(dstOperational);
                // Check if repair ended
                if(DevSt == dstDamaged and TmrRepair.IsEnded()) {
                    if(TmrReload.IsEnded()) SetState(dstOperational);
                    else SetState(dstReload);
                }
            }
        } // EVT_EVERY_SECOND

        if(Evt & EVT_IR_RX) {
            Uart.Printf("IR RX\r");
//            irRx.LastPkt.Print();
            DevState_t DevSt = (DevState_t)DevInfo.State;
            bool GroupOk = (irRx.LastPkt.Group == Settings.Group); // Ignore shots from other groups
            bool IdOk = (irRx.LastPkt.ID != Settings.ID);  // Ignore self shots
            bool StateOk = ANY_OF_3(DevSt, dstOperational, dstReload, dstDamaged); // Ignore if not in damage enabled state
            bool TypeOk = (Settings.Type == dtTank) or (irRx.LastPkt.IsTank == 1); // Tank is vulnerable to everybody, Target/Turret is vulnerable to tanks only
            if(GroupOk and IdOk and StateOk and TypeOk) {
                // Disable LED events if any, otherwise they could switch state to something wrong
                LedRGB.SetupSeqEndEvt(nullptr);
                LedR.SetupSeqEndEvt(nullptr);
                // Make damage
                if(irRx.LastPkt.Damage <= DevInfo.HitCnt) DevInfo.HitCnt -= irRx.LastPkt.Damage;
                else DevInfo.HitCnt = 0;
                // Save new hit cnt
                Settings.SetHitCount(DevInfo.HitCnt);
                // Save ID of attacker
                DevInfo.LastAttacker = irRx.LastPkt.ID;
                // Switch state depending on hits left
                if(DevInfo.HitCnt == 0) { // Destroyed
                    Out1.SetHi();
                    Out2.SetLo();
                    Out3.SetHi();
                    LedRGB.SetupSeqEndEvt(chThdGetSelfX(), EVT_LEDSEQEND_DSTRYD);
                }
                else {
                    Out1.SetLo();
                    Out2.SetHi();
                    Out3.SetHi();
                    LedRGB.SetupSeqEndEvt(chThdGetSelfX(), EVT_LEDSEQEND_DMGD);
                    TmrRepair.StartOrRestart(Settings.RepairTime);
                }
                // === Setup indication ===
                LedRGB.StartOrRestart(lsqHit);
                if(Settings.Type == dtTank) {
                    LedY1.StartOrRestart(lsqSmoothHit);
                    LedY2.StartOrRestart(lsqSmoothHit);
                    LedY3.StartOrRestart(lsqSmoothHit);
                    LedY4.StartOrRestart(lsqSmoothHit);
                }
                else {
                    LedY1.StartOrRestart(lsqSmoothHit);
                    LedY2.StartOrRestart(lsqSmoothHit);
                    LedR.StartOrRestart(lsqOff);
                    LedG.StartOrContinue(lsqOff);
                }
            } // if ok
        } // if IR RX

        if(Evt & EVT_INPUT) {
            Uart.Printf("EvtFire, ShotCnt=%u\r", DevInfo.ShotCnt);
            if((DevState_t)DevInfo.State == dstOperational) {
                if(DevInfo.ShotCnt != 0) {
                    // Fire!
                    // Decrease shot cnt if not infinity
                    if(DevInfo.ShotCnt != 255) {
                        DevInfo.ShotCnt--;
                        // Save new shot cnt if device is tank
                        if(Settings.Type == dtTank) Settings.SetShotCount(DevInfo.ShotCnt);
                    }
                    // ==== Setup IR Pkt Tx ====
                    irPktTx.ID = Settings.ID;
                    irPktTx.Group = Settings.Group;
                    irPktTx.Damage = Settings.IRDamage;
                    irPktTx.IsTank = (Settings.Type == dtTank)? 1 : 0;
                    irPktTx.ControlSum = irPktTx.CalculateControlSum();
    //                irPktTx.Print();
                    irLed.TransmitWord(irPktTx.Word, Settings.IRPower);
                    // Start reload timer
                    TmrReload.StartOrRestart(Settings.ReloadTime);
                    // === Set up indication ===
                    if(Settings.Type == dtTank) {
                        LedFire.StartOrRestart(lsqFire);
                        if(DevInfo.ShotCnt == 0) SetState(dstOperational);
                        else SetState(dstReload);
                    }
                    else {
                        LedR.StartOrRestart(lsqFire);
                        LedR.SetupSeqEndEvt(chThdGetSelfX(), EVT_LEDSEQEND_FIRE);
                    }
                } // If shot cnt
            } // if state
        } // EVT_INPUT
    }
} Device;
#endif

int main(void) {
    // ==== Setup clock frequency ====
    Clk.SetupBusDividers(ahbDiv1, apbDiv1, apbDiv1);
    Clk.SwitchToHSE();
    Clk.UpdateFreqValues();
    // Init OS
    halInit();
    chSysInit();
    // ==== Init hardware ====
    App.InitThread();
    Uart.Init(115200, UART_GPIO, UART_TX_PIN, UART_GPIO, UART_RX_PIN);

    i2c3.Init();
    Settings.Init();
    Settings.Read();
    DevInfo.Type = Settings.Type;
    DevInfo.Group = Settings.Group;
    DevInfo.HitCnt = Settings.HitCount;
    DevInfo.ShotCnt = Settings.ShotCount;

    Uart.Printf("\r%S %S ID=%u; Type=%u; State=%u\r", APP_NAME, BUILD_TIME, Settings.ID, Settings.Type, Settings.State);
    Clk.PrintFreqs();

    // LEDs
    LedRGB.Init();
    LedFire.Init();
    LedY1.Init();
    LedY2.Init();
    LedY3.Init();
    LedY4.Init();
    // IR
    irLed.Init();
    irRx.Init();

    // Input
    PinSensors.Init();
    // Outputs
    Out1.Init();
    Out2.Init();
    Out3.Init();

    LedFire.StartOrRestart(lsqSmoothStart);
    chThdSleepMilliseconds(99);
    LedY1.StartOrRestart(lsqSmoothStart);
    chThdSleepMilliseconds(99);
    LedY2.StartOrRestart(lsqSmoothStart);
    chThdSleepMilliseconds(99);
    LedY3.StartOrRestart(lsqSmoothStart);
    chThdSleepMilliseconds(99);
    LedY4.StartOrRestart(lsqSmoothStart);
    chThdSleepMilliseconds(1800);

    if(Settings.State > 5) Settings.State = 1;  // in case of incorrect saved data
    Device.SetState((DevState_t)Settings.State);

    if(Radio.Init() != OK) {
        LedRGB.StartOrRestart(lsqFailure);
        chThdSleepMilliseconds(5400);
    }

    // Timer
    TmrEverySecond.InitAndStart();

    // Main cycle
    App.ITask();
}

__noreturn
void App_t::ITask() {
    while(true) {
        uint32_t Evt = chEvtWaitAny(ALL_EVENTS);
        // Handle specific events
        Device.EvtHandler(Evt);

        if(Evt & EVT_NEW_PARAM) {
            Convert::WordBytes_t wb;
            while(ParamBuf.Get(&wb) == OK) {
                Uart.Printf("Param %u %u\r", wb.b[0], wb.b[1]);
                switch(wb.b[0]) {
                    case 20:
                        DevInfo.Group = wb.b[1];
                        Settings.SetGroup(wb.b[1]);
                        break;

                    case 21:
                        DevInfo.HitCnt = wb.b[1];
                        Settings.SetHitCount(wb.b[1]);
                        break;

                    case 22:
                        DevInfo.ShotCnt = wb.b[1];
                        if((DevState_t)DevInfo.State == dstOperational) {
                            if(DevInfo.ShotCnt != 0) Out3.SetLo();
                            else Out3.SetHi();
                        }
                        // Restart turret shooting
                        if(Settings.Type == dtTurret) App.SignalEvt(EVT_INPUT);
                        // Save new shot cnt
                        Settings.SetShotCount(DevInfo.ShotCnt);
                        break;

                    case 23:
                        DevInfo.State = wb.b[1];
                        Device.SetState((DevState_t)wb.b[1]);
                        break;

                    case 30: Settings.SetIRPower(wb.b[1]); break;
                    case 31: Settings.SetIRDamage(wb.b[1]); break;
                    case 32: Settings.SetReloadTime(wb.b[1]); break;
                    case 33: Settings.SetRepairTime(wb.b[1]); break;
                    default: break;
                } // switch
            } // while get
        }

        if(Evt & EVT_UART_NEW_CMD) {
            OnCmd((Shell_t*)&Uart);
            Uart.SignalCmdProcessed();
        }
    } // while true
}

void ProcessInputPin(PinSnsState_t *PState, uint32_t Len) {
    if(PState[0] == pssFalling) App.SignalEvt(EVT_INPUT);
}

void OnRadioRx() {
    if(Radio.PktRx.ParamID != 0) {
        // Put data to param bufer to send it to app
        Convert::WordBytes_t wb;
        wb.b[0] = Radio.PktRx.ParamID;
        wb.b[1] = Radio.PktRx.ParamValue;
        ParamBuf.PutAnyway(wb);
        App.SignalEvt(EVT_NEW_PARAM);
//        Uart.Printf("Param %u %u\r", Radio.PktRx.ParamID, Radio.PktRx.ParamValue);
    }
    // Copy actual data to the txpkt
    Radio.PktTx.DevInfo.DWord = DevInfo.DWord;
}

#if 1 // ======================= Command processing ============================
void App_t::OnCmd(Shell_t *PShell) {
	Cmd_t *PCmd = &PShell->Cmd;
    __unused int32_t dw32 = 0;  // May be unused in some configurations
    Uart.Printf("\r%S\r", PCmd->Name);
    // Handle command
    if(PCmd->NameIs("Ping")) PShell->Ack(OK);

    // ==== ID ====
    else if(PCmd->NameIs("GetID")) PShell->Reply("ID", Settings.ID);
    else if(PCmd->NameIs("SetID")) {
        if(PCmd->GetNextInt32(&dw32) != OK) { PShell->Ack(CMD_ERROR); return; }
        uint8_t r = Settings.SetID(dw32);
        PShell->Ack(r);
    }

    // ==== Type ====
    else if(PCmd->NameIs("GetType")) PShell->Reply("Type", Settings.Type);
    else if(PCmd->NameIs("SetType")) {
        if(PCmd->GetNextInt32(&dw32) != OK) { PShell->Ack(CMD_ERROR); return; }
        uint8_t r = Settings.SetType((DeviceType_t)dw32);
        PShell->Ack(r);
    }

    else if(PCmd->NameIs("GetSettings")) Settings.Print();

    else if(PCmd->NameIs("SetSettings")) {
        uint8_t Arr[(SETTINGS_SZ - 1)];
        if(PCmd->GetArray(Arr, (SETTINGS_SZ - 1)) != OK) { PShell->Ack(CMD_ERROR); return; }
        uint8_t r = Settings.SetAllExceptID(Arr);
        PShell->Ack(r);
    }

    else if(PCmd->NameIs("SetState")) {
        if(PCmd->GetNextInt32(&dw32) != OK) { PShell->Ack(CMD_ERROR); return; }
        Device.SetState((DevState_t)dw32);
    }

    else if(PCmd->NameIs("Fire")) {
        App.SignalEvt(EVT_INPUT);
    }

    else PShell->Ack(CMD_UNKNOWN);
}
#endif
