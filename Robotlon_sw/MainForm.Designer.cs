﻿namespace Robotlon_sw {
    partial class Mainform {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainform));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DgvDevices = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.хитыDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.выстрелыDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.мишениDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.состояниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCommState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataSet1 = new System.Data.DataSet();
            this.TableDevices = new System.Data.DataTable();
            this.ColID = new System.Data.DataColumn();
            this.ColType = new System.Data.DataColumn();
            this.ColName = new System.Data.DataColumn();
            this.ColGroup = new System.Data.DataColumn();
            this.ColHitCnt = new System.Data.DataColumn();
            this.ColShotCnt = new System.Data.DataColumn();
            this.ColTargetsHitCnt = new System.Data.DataColumn();
            this.ColState = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.TableGroups = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.TableDevStates = new System.Data.DataTable();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.TableDevTypes = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.BtnSendState = new System.Windows.Forms.Button();
            this.CmbGroup = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnSetShotCnt = new System.Windows.Forms.Button();
            this.CmbShotCnt = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.BtnSetHitCnt = new System.Windows.Forms.Button();
            this.CmbHitCnt = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CmbHitRecipient = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnResetTargetCount = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.CmbDevState = new System.Windows.Forms.ComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnSendRepairTime = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.CmbRepairTime = new System.Windows.Forms.ComboBox();
            this.BtnSendReloadTime = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.CmbReloadTime = new System.Windows.Forms.ComboBox();
            this.BtnSendIRDamage = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.CmbIRDamage = new System.Windows.Forms.ComboBox();
            this.BtnSendIRPwr = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.CmbIRPwr = new System.Windows.Forms.ComboBox();
            this.CmbSingleGroupFilter = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnSingleResetTargetCount = new System.Windows.Forms.Button();
            this.BtnSingleSendState = new System.Windows.Forms.Button();
            this.CmbSingleState = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.BtnSingleSetShotCnt = new System.Windows.Forms.Button();
            this.CmbSingleShotCnt = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnSingleSetHitCnt = new System.Windows.Forms.Button();
            this.CmbSingleHitCnt = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.BtnSelect2 = new System.Windows.Forms.Button();
            this.BtnSelect1 = new System.Windows.Forms.Button();
            this.TxtbName2 = new System.Windows.Forms.TextBox();
            this.TxtbName1 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.ChbTimerOn = new System.Windows.Forms.CheckBox();
            this.BtnTmrReset = new System.Windows.Forms.Button();
            this.TxtbTimer = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.TxtbState4 = new System.Windows.Forms.TextBox();
            this.TxtbState3 = new System.Windows.Forms.TextBox();
            this.TxtbState2 = new System.Windows.Forms.TextBox();
            this.TxtbState1 = new System.Windows.Forms.TextBox();
            this.RBtnState4 = new System.Windows.Forms.RadioButton();
            this.RBtnState3 = new System.Windows.Forms.RadioButton();
            this.RBtnState2 = new System.Windows.Forms.RadioButton();
            this.RBtnState1 = new System.Windows.Forms.RadioButton();
            this.TxtbCaption = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ChbShowScoreboard = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.BtnStartFight = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.CmbSFTurretHits = new System.Windows.Forms.ComboBox();
            this.CmbSFTargetHits = new System.Windows.Forms.ComboBox();
            this.CmbSFShotsTanks = new System.Windows.Forms.ComboBox();
            this.CmbSFTankHits = new System.Windows.Forms.ComboBox();
            this.BtnPrepare = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.BtnStopFight = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.DgvDevInGroup = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DgvGrpList = new System.Windows.Forms.DataGridView();
            this.colGroupNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BtnDeleteGroup = new System.Windows.Forms.Button();
            this.BtnAddGroup = new System.Windows.Forms.Button();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.типDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.названиеDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.группаDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Хиты = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Выстрелы = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Состояние = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.TimerScore = new System.Windows.Forms.Timer(this.components);
            this.ChbShowRight = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevTypes)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDevInGroup)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvGrpList)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 524);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1147, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(88, 17);
            this.StatusLabel.Text = "Not Connected";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1147, 524);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DgvDevices);
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1139, 498);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Управление устройствами";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DgvDevices
            // 
            this.DgvDevices.AllowUserToAddRows = false;
            this.DgvDevices.AllowUserToDeleteRows = false;
            this.DgvDevices.AllowUserToResizeRows = false;
            this.DgvDevices.AutoGenerateColumns = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvDevices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DgvDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDevices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.типDataGridViewTextBoxColumn,
            this.названиеDataGridViewTextBoxColumn,
            this.группаDataGridViewTextBoxColumn,
            this.хитыDataGridViewTextBoxColumn,
            this.выстрелыDataGridViewTextBoxColumn,
            this.мишениDataGridViewTextBoxColumn,
            this.состояниеDataGridViewTextBoxColumn,
            this.ColCommState});
            this.DgvDevices.DataMember = "TableDevices";
            this.DgvDevices.DataSource = this.dataSet1;
            this.DgvDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvDevices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DgvDevices.Location = new System.Drawing.Point(3, 3);
            this.DgvDevices.MultiSelect = false;
            this.DgvDevices.Name = "DgvDevices";
            this.DgvDevices.RowHeadersVisible = false;
            this.DgvDevices.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvDevices.Size = new System.Drawing.Size(800, 492);
            this.DgvDevices.TabIndex = 5;
            this.DgvDevices.VirtualMode = true;
            this.DgvDevices.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DgvDevices_CellPainting);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 43;
            // 
            // типDataGridViewTextBoxColumn
            // 
            this.типDataGridViewTextBoxColumn.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn.Name = "типDataGridViewTextBoxColumn";
            this.типDataGridViewTextBoxColumn.ReadOnly = true;
            this.типDataGridViewTextBoxColumn.Width = 51;
            // 
            // названиеDataGridViewTextBoxColumn
            // 
            this.названиеDataGridViewTextBoxColumn.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn.Name = "названиеDataGridViewTextBoxColumn";
            this.названиеDataGridViewTextBoxColumn.Width = 82;
            // 
            // группаDataGridViewTextBoxColumn
            // 
            this.группаDataGridViewTextBoxColumn.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn.Name = "группаDataGridViewTextBoxColumn";
            this.группаDataGridViewTextBoxColumn.ReadOnly = true;
            this.группаDataGridViewTextBoxColumn.Width = 67;
            // 
            // хитыDataGridViewTextBoxColumn
            // 
            this.хитыDataGridViewTextBoxColumn.DataPropertyName = "Хиты";
            this.хитыDataGridViewTextBoxColumn.HeaderText = "Хиты";
            this.хитыDataGridViewTextBoxColumn.Name = "хитыDataGridViewTextBoxColumn";
            this.хитыDataGridViewTextBoxColumn.ReadOnly = true;
            this.хитыDataGridViewTextBoxColumn.Width = 58;
            // 
            // выстрелыDataGridViewTextBoxColumn
            // 
            this.выстрелыDataGridViewTextBoxColumn.DataPropertyName = "Выстрелы";
            this.выстрелыDataGridViewTextBoxColumn.HeaderText = "Выстрелы";
            this.выстрелыDataGridViewTextBoxColumn.Name = "выстрелыDataGridViewTextBoxColumn";
            this.выстрелыDataGridViewTextBoxColumn.ReadOnly = true;
            this.выстрелыDataGridViewTextBoxColumn.Width = 84;
            // 
            // мишениDataGridViewTextBoxColumn
            // 
            this.мишениDataGridViewTextBoxColumn.DataPropertyName = "Мишени";
            this.мишениDataGridViewTextBoxColumn.HeaderText = "Мишени";
            this.мишениDataGridViewTextBoxColumn.Name = "мишениDataGridViewTextBoxColumn";
            this.мишениDataGridViewTextBoxColumn.ReadOnly = true;
            this.мишениDataGridViewTextBoxColumn.Width = 73;
            // 
            // состояниеDataGridViewTextBoxColumn
            // 
            this.состояниеDataGridViewTextBoxColumn.DataPropertyName = "Состояние";
            this.состояниеDataGridViewTextBoxColumn.HeaderText = "Состояние";
            this.состояниеDataGridViewTextBoxColumn.Name = "состояниеDataGridViewTextBoxColumn";
            this.состояниеDataGridViewTextBoxColumn.ReadOnly = true;
            this.состояниеDataGridViewTextBoxColumn.Width = 86;
            // 
            // ColCommState
            // 
            this.ColCommState.DataPropertyName = "ColCommState";
            this.ColCommState.HeaderText = "Связь";
            this.ColCommState.Name = "ColCommState";
            this.ColCommState.ReadOnly = true;
            this.ColCommState.Width = 101;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("GroupNames", "TableGroups", "TableDevices", new string[] {
                        "ColGroupName"}, new string[] {
                        "Группа"}, false)});
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.TableDevices,
            this.TableGroups,
            this.TableDevStates,
            this.TableDevTypes});
            // 
            // TableDevices
            // 
            this.TableDevices.Columns.AddRange(new System.Data.DataColumn[] {
            this.ColID,
            this.ColType,
            this.ColName,
            this.ColGroup,
            this.ColHitCnt,
            this.ColShotCnt,
            this.ColTargetsHitCnt,
            this.ColState,
            this.dataColumn3});
            this.TableDevices.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("GroupNames", "TableGroups", new string[] {
                        "ColGroupName"}, new string[] {
                        "Группа"}, System.Data.AcceptRejectRule.None, System.Data.Rule.SetDefault, System.Data.Rule.Cascade)});
            this.TableDevices.TableName = "TableDevices";
            // 
            // ColID
            // 
            this.ColID.AllowDBNull = false;
            this.ColID.ColumnName = "ID";
            this.ColID.DataType = typeof(int);
            // 
            // ColType
            // 
            this.ColType.ColumnName = "Тип";
            // 
            // ColName
            // 
            this.ColName.ColumnName = "Название";
            // 
            // ColGroup
            // 
            this.ColGroup.ColumnName = "Группа";
            // 
            // ColHitCnt
            // 
            this.ColHitCnt.ColumnName = "Хиты";
            // 
            // ColShotCnt
            // 
            this.ColShotCnt.ColumnName = "Выстрелы";
            // 
            // ColTargetsHitCnt
            // 
            this.ColTargetsHitCnt.ColumnName = "Мишени";
            // 
            // ColState
            // 
            this.ColState.ColumnName = "Состояние";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "ColCommState";
            // 
            // TableGroups
            // 
            this.TableGroups.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.TableGroups.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ColGroupName"}, false),
            new System.Data.UniqueConstraint("Constraint2", new string[] {
                        "ColGroupID"}, true)});
            this.TableGroups.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn2};
            this.TableGroups.TableName = "TableGroups";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ColGroupName";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "ColGroupID";
            // 
            // TableDevStates
            // 
            this.TableDevStates.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn4,
            this.dataColumn7});
            this.TableDevStates.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ColDevSt"}, true)});
            this.TableDevStates.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn4};
            this.TableDevStates.TableName = "TableDevStates";
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "ColDevSt";
            // 
            // dataColumn7
            // 
            this.dataColumn7.AllowDBNull = false;
            this.dataColumn7.ColumnName = "ColDevStID";
            // 
            // TableDevTypes
            // 
            this.TableDevTypes.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn9});
            this.TableDevTypes.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ColDevTypeID"}, true)});
            this.TableDevTypes.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn9};
            this.TableDevTypes.TableName = "TableDevTypes";
            // 
            // dataColumn6
            // 
            this.dataColumn6.AllowDBNull = false;
            this.dataColumn6.ColumnName = "ColDevType";
            // 
            // dataColumn9
            // 
            this.dataColumn9.AllowDBNull = false;
            this.dataColumn9.ColumnName = "ColDevTypeID";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControl2.Location = new System.Drawing.Point(803, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(333, 492);
            this.tabControl2.TabIndex = 6;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.BtnSendState);
            this.tabPage3.Controls.Add(this.CmbGroup);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.BtnResetTargetCount);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.CmbDevState);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(325, 466);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Группа";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // BtnSendState
            // 
            this.BtnSendState.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSendState.Location = new System.Drawing.Point(230, 235);
            this.BtnSendState.Name = "BtnSendState";
            this.BtnSendState.Size = new System.Drawing.Size(34, 21);
            this.BtnSendState.TabIndex = 25;
            this.BtnSendState.UseVisualStyleBackColor = true;
            this.BtnSendState.Click += new System.EventHandler(this.BtnSendState_Click);
            // 
            // CmbGroup
            // 
            this.CmbGroup.DataSource = this.dataSet1;
            this.CmbGroup.DisplayMember = "TableGroups.ColGroupName";
            this.CmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbGroup.FormattingEnabled = true;
            this.CmbGroup.Location = new System.Drawing.Point(60, 38);
            this.CmbGroup.Name = "CmbGroup";
            this.CmbGroup.Size = new System.Drawing.Size(140, 21);
            this.CmbGroup.TabIndex = 24;
            this.CmbGroup.TextChanged += new System.EventHandler(this.CmbGroup_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Группа:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(319, 18);
            this.label1.TabIndex = 22;
            this.label1.Text = "Команду получат все устройства в группе.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.BtnSetShotCnt);
            this.groupBox3.Controls.Add(this.CmbShotCnt);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(179, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(140, 118);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Количество выстрелов";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Только для танков";
            // 
            // BtnSetShotCnt
            // 
            this.BtnSetShotCnt.Location = new System.Drawing.Point(36, 89);
            this.BtnSetShotCnt.Name = "BtnSetShotCnt";
            this.BtnSetShotCnt.Size = new System.Drawing.Size(75, 23);
            this.BtnSetShotCnt.TabIndex = 4;
            this.BtnSetShotCnt.Text = "Задать";
            this.BtnSetShotCnt.UseVisualStyleBackColor = true;
            this.BtnSetShotCnt.Click += new System.EventHandler(this.BtnSetShotCnt_Click);
            // 
            // CmbShotCnt
            // 
            this.CmbShotCnt.FormattingEnabled = true;
            this.CmbShotCnt.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbShotCnt.Location = new System.Drawing.Point(65, 48);
            this.CmbShotCnt.Name = "CmbShotCnt";
            this.CmbShotCnt.Size = new System.Drawing.Size(64, 21);
            this.CmbShotCnt.TabIndex = 3;
            this.CmbShotCnt.Text = "18";
            this.CmbShotCnt.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Сколько:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.BtnSetHitCnt);
            this.groupBox4.Controls.Add(this.CmbHitCnt);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.CmbHitRecipient);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(9, 75);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(164, 118);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Количество хитов";
            // 
            // BtnSetHitCnt
            // 
            this.BtnSetHitCnt.Location = new System.Drawing.Point(51, 89);
            this.BtnSetHitCnt.Name = "BtnSetHitCnt";
            this.BtnSetHitCnt.Size = new System.Drawing.Size(75, 23);
            this.BtnSetHitCnt.TabIndex = 4;
            this.BtnSetHitCnt.Text = "Задать";
            this.BtnSetHitCnt.UseVisualStyleBackColor = true;
            this.BtnSetHitCnt.Click += new System.EventHandler(this.BtnSetHitCnt_Click);
            // 
            // CmbHitCnt
            // 
            this.CmbHitCnt.FormattingEnabled = true;
            this.CmbHitCnt.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbHitCnt.Location = new System.Drawing.Point(65, 48);
            this.CmbHitCnt.Name = "CmbHitCnt";
            this.CmbHitCnt.Size = new System.Drawing.Size(91, 21);
            this.CmbHitCnt.TabIndex = 3;
            this.CmbHitCnt.Text = "4";
            this.CmbHitCnt.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Сколько:";
            // 
            // CmbHitRecipient
            // 
            this.CmbHitRecipient.DataSource = this.dataSet1;
            this.CmbHitRecipient.DisplayMember = "TableDevTypes.ColDevType";
            this.CmbHitRecipient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbHitRecipient.FormattingEnabled = true;
            this.CmbHitRecipient.Location = new System.Drawing.Point(65, 18);
            this.CmbHitRecipient.Name = "CmbHitRecipient";
            this.CmbHitRecipient.Size = new System.Drawing.Size(91, 21);
            this.CmbHitRecipient.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Всем, кто:";
            // 
            // BtnResetTargetCount
            // 
            this.BtnResetTargetCount.Location = new System.Drawing.Point(9, 199);
            this.BtnResetTargetCount.Name = "BtnResetTargetCount";
            this.BtnResetTargetCount.Size = new System.Drawing.Size(310, 23);
            this.BtnResetTargetCount.TabIndex = 19;
            this.BtnResetTargetCount.Text = "Сбросить количество поражённых мишеней";
            this.BtnResetTargetCount.UseVisualStyleBackColor = true;
            this.BtnResetTargetCount.Click += new System.EventHandler(this.BtnResetTargetCount_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Состояние:";
            // 
            // CmbDevState
            // 
            this.CmbDevState.DataSource = this.dataSet1;
            this.CmbDevState.DisplayMember = "TableDevStates.ColDevSt";
            this.CmbDevState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDevState.FormattingEnabled = true;
            this.CmbDevState.Location = new System.Drawing.Point(73, 235);
            this.CmbDevState.Name = "CmbDevState";
            this.CmbDevState.Size = new System.Drawing.Size(151, 21);
            this.CmbDevState.TabIndex = 15;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.CmbSingleGroupFilter);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.BtnSingleResetTargetCount);
            this.tabPage4.Controls.Add(this.BtnSingleSendState);
            this.tabPage4.Controls.Add(this.CmbSingleState);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(325, 466);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Одиночный";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnSendRepairTime);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.CmbRepairTime);
            this.groupBox2.Controls.Add(this.BtnSendReloadTime);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.CmbReloadTime);
            this.groupBox2.Controls.Add(this.BtnSendIRDamage);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.CmbIRDamage);
            this.groupBox2.Controls.Add(this.BtnSendIRPwr);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.CmbIRPwr);
            this.groupBox2.Location = new System.Drawing.Point(3, 291);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 169);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройки";
            // 
            // BtnSendRepairTime
            // 
            this.BtnSendRepairTime.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSendRepairTime.Location = new System.Drawing.Point(273, 134);
            this.BtnSendRepairTime.Name = "BtnSendRepairTime";
            this.BtnSendRepairTime.Size = new System.Drawing.Size(34, 21);
            this.BtnSendRepairTime.TabIndex = 41;
            this.BtnSendRepairTime.UseVisualStyleBackColor = true;
            this.BtnSendRepairTime.Click += new System.EventHandler(this.BtnSendRepairTime_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 137);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "Время ремонта (0...255 с)";
            // 
            // CmbRepairTime
            // 
            this.CmbRepairTime.FormattingEnabled = true;
            this.CmbRepairTime.Items.AddRange(new object[] {
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "128",
            "200",
            "255"});
            this.CmbRepairTime.Location = new System.Drawing.Point(176, 134);
            this.CmbRepairTime.Name = "CmbRepairTime";
            this.CmbRepairTime.Size = new System.Drawing.Size(91, 21);
            this.CmbRepairTime.TabIndex = 39;
            this.CmbRepairTime.Text = "4";
            this.CmbRepairTime.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // BtnSendReloadTime
            // 
            this.BtnSendReloadTime.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSendReloadTime.Location = new System.Drawing.Point(273, 97);
            this.BtnSendReloadTime.Name = "BtnSendReloadTime";
            this.BtnSendReloadTime.Size = new System.Drawing.Size(34, 21);
            this.BtnSendReloadTime.TabIndex = 38;
            this.BtnSendReloadTime.UseVisualStyleBackColor = true;
            this.BtnSendReloadTime.Click += new System.EventHandler(this.BtnSendReloadTime_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(160, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Время перезарядки (0...255 с)";
            // 
            // CmbReloadTime
            // 
            this.CmbReloadTime.FormattingEnabled = true;
            this.CmbReloadTime.Items.AddRange(new object[] {
            "2",
            "4",
            "6",
            "8",
            "10",
            "12",
            "14",
            "16"});
            this.CmbReloadTime.Location = new System.Drawing.Point(176, 97);
            this.CmbReloadTime.Name = "CmbReloadTime";
            this.CmbReloadTime.Size = new System.Drawing.Size(91, 21);
            this.CmbReloadTime.TabIndex = 36;
            this.CmbReloadTime.Text = "4";
            this.CmbReloadTime.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // BtnSendIRDamage
            // 
            this.BtnSendIRDamage.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSendIRDamage.Location = new System.Drawing.Point(273, 61);
            this.BtnSendIRDamage.Name = "BtnSendIRDamage";
            this.BtnSendIRDamage.Size = new System.Drawing.Size(34, 21);
            this.BtnSendIRDamage.TabIndex = 35;
            this.BtnSendIRDamage.UseVisualStyleBackColor = true;
            this.BtnSendIRDamage.Click += new System.EventHandler(this.BtnSendIRDamage_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(57, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Поражение ИК (0...3)";
            // 
            // CmbIRDamage
            // 
            this.CmbIRDamage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbIRDamage.FormattingEnabled = true;
            this.CmbIRDamage.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.CmbIRDamage.Location = new System.Drawing.Point(176, 61);
            this.CmbIRDamage.Name = "CmbIRDamage";
            this.CmbIRDamage.Size = new System.Drawing.Size(91, 21);
            this.CmbIRDamage.TabIndex = 33;
            // 
            // BtnSendIRPwr
            // 
            this.BtnSendIRPwr.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSendIRPwr.Location = new System.Drawing.Point(273, 25);
            this.BtnSendIRPwr.Name = "BtnSendIRPwr";
            this.BtnSendIRPwr.Size = new System.Drawing.Size(34, 21);
            this.BtnSendIRPwr.TabIndex = 32;
            this.BtnSendIRPwr.UseVisualStyleBackColor = true;
            this.BtnSendIRPwr.Click += new System.EventHandler(this.BtnSendIRPwr_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(50, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Мощность ИК (0...255)";
            // 
            // CmbIRPwr
            // 
            this.CmbIRPwr.FormattingEnabled = true;
            this.CmbIRPwr.Items.AddRange(new object[] {
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "128",
            "200",
            "255"});
            this.CmbIRPwr.Location = new System.Drawing.Point(176, 25);
            this.CmbIRPwr.Name = "CmbIRPwr";
            this.CmbIRPwr.Size = new System.Drawing.Size(91, 21);
            this.CmbIRPwr.TabIndex = 4;
            this.CmbIRPwr.Text = "4";
            this.CmbIRPwr.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // CmbSingleGroupFilter
            // 
            this.CmbSingleGroupFilter.DataSource = this.dataSet1;
            this.CmbSingleGroupFilter.DisplayMember = "TableGroups.ColGroupName";
            this.CmbSingleGroupFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSingleGroupFilter.FormattingEnabled = true;
            this.CmbSingleGroupFilter.Location = new System.Drawing.Point(60, 38);
            this.CmbSingleGroupFilter.Name = "CmbSingleGroupFilter";
            this.CmbSingleGroupFilter.Size = new System.Drawing.Size(140, 21);
            this.CmbSingleGroupFilter.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Группа:";
            // 
            // BtnSingleResetTargetCount
            // 
            this.BtnSingleResetTargetCount.Location = new System.Drawing.Point(6, 191);
            this.BtnSingleResetTargetCount.Name = "BtnSingleResetTargetCount";
            this.BtnSingleResetTargetCount.Size = new System.Drawing.Size(310, 23);
            this.BtnSingleResetTargetCount.TabIndex = 33;
            this.BtnSingleResetTargetCount.Text = "Сбросить количество поражённых мишеней";
            this.BtnSingleResetTargetCount.UseVisualStyleBackColor = true;
            this.BtnSingleResetTargetCount.Click += new System.EventHandler(this.BtnSingleResetTargetCount_Click);
            // 
            // BtnSingleSendState
            // 
            this.BtnSingleSendState.Image = global::Robotlon_sw.Properties.Resources.Checked2;
            this.BtnSingleSendState.Location = new System.Drawing.Point(166, 264);
            this.BtnSingleSendState.Name = "BtnSingleSendState";
            this.BtnSingleSendState.Size = new System.Drawing.Size(34, 21);
            this.BtnSingleSendState.TabIndex = 31;
            this.BtnSingleSendState.UseVisualStyleBackColor = true;
            this.BtnSingleSendState.Click += new System.EventHandler(this.BtnSingleSendState_Click);
            // 
            // CmbSingleState
            // 
            this.CmbSingleState.DataSource = this.dataSet1;
            this.CmbSingleState.DisplayMember = "TableDevStates.ColDevSt";
            this.CmbSingleState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSingleState.FormattingEnabled = true;
            this.CmbSingleState.Location = new System.Drawing.Point(7, 264);
            this.CmbSingleState.Name = "CmbSingleState";
            this.CmbSingleState.Size = new System.Drawing.Size(153, 21);
            this.CmbSingleState.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Состояние устройства";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.BtnSingleSetShotCnt);
            this.groupBox7.Controls.Add(this.CmbSingleShotCnt);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Location = new System.Drawing.Point(176, 92);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(140, 93);
            this.groupBox7.TabIndex = 24;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Количество выстрелов";
            // 
            // BtnSingleSetShotCnt
            // 
            this.BtnSingleSetShotCnt.Location = new System.Drawing.Point(38, 60);
            this.BtnSingleSetShotCnt.Name = "BtnSingleSetShotCnt";
            this.BtnSingleSetShotCnt.Size = new System.Drawing.Size(75, 23);
            this.BtnSingleSetShotCnt.TabIndex = 4;
            this.BtnSingleSetShotCnt.Text = "Задать";
            this.BtnSingleSetShotCnt.UseVisualStyleBackColor = true;
            this.BtnSingleSetShotCnt.Click += new System.EventHandler(this.BtnSingleSetShotCnt_Click);
            // 
            // CmbSingleShotCnt
            // 
            this.CmbSingleShotCnt.FormattingEnabled = true;
            this.CmbSingleShotCnt.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSingleShotCnt.Location = new System.Drawing.Point(67, 19);
            this.CmbSingleShotCnt.Name = "CmbSingleShotCnt";
            this.CmbSingleShotCnt.Size = new System.Drawing.Size(64, 21);
            this.CmbSingleShotCnt.TabIndex = 3;
            this.CmbSingleShotCnt.Text = "18";
            this.CmbSingleShotCnt.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Сколько:";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(319, 18);
            this.label10.TabIndex = 23;
            this.label10.Text = "Команду получит выбранное в таблице устройство.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnSingleSetHitCnt);
            this.groupBox1.Controls.Add(this.CmbSingleHitCnt);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(6, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 93);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Количество хитов";
            // 
            // BtnSingleSetHitCnt
            // 
            this.BtnSingleSetHitCnt.Location = new System.Drawing.Point(52, 60);
            this.BtnSingleSetHitCnt.Name = "BtnSingleSetHitCnt";
            this.BtnSingleSetHitCnt.Size = new System.Drawing.Size(75, 23);
            this.BtnSingleSetHitCnt.TabIndex = 4;
            this.BtnSingleSetHitCnt.Text = "Задать";
            this.BtnSingleSetHitCnt.UseVisualStyleBackColor = true;
            this.BtnSingleSetHitCnt.Click += new System.EventHandler(this.BtnSingleSetHitCnt_Click);
            // 
            // CmbSingleHitCnt
            // 
            this.CmbSingleHitCnt.FormattingEnabled = true;
            this.CmbSingleHitCnt.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSingleHitCnt.Location = new System.Drawing.Point(66, 19);
            this.CmbSingleHitCnt.Name = "CmbSingleHitCnt";
            this.CmbSingleHitCnt.Size = new System.Drawing.Size(91, 21);
            this.CmbSingleHitCnt.TabIndex = 3;
            this.CmbSingleHitCnt.Text = "4";
            this.CmbSingleHitCnt.TextChanged += new System.EventHandler(this.CheckOnTextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Сколько:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.ChbShowRight);
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Controls.Add(this.groupBox9);
            this.tabPage5.Controls.Add(this.groupBox8);
            this.tabPage5.Controls.Add(this.TxtbCaption);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.ChbShowScoreboard);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(325, 466);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Табло";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.BtnSelect2);
            this.groupBox10.Controls.Add(this.BtnSelect1);
            this.groupBox10.Controls.Add(this.TxtbName2);
            this.groupBox10.Controls.Add(this.TxtbName1);
            this.groupBox10.Location = new System.Drawing.Point(23, 241);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(299, 96);
            this.groupBox10.TabIndex = 12;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выбор участников";
            // 
            // BtnSelect2
            // 
            this.BtnSelect2.Image = global::Robotlon_sw.Properties.Resources.RightRight;
            this.BtnSelect2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSelect2.Location = new System.Drawing.Point(110, 17);
            this.BtnSelect2.Name = "BtnSelect2";
            this.BtnSelect2.Size = new System.Drawing.Size(84, 23);
            this.BtnSelect2.TabIndex = 12;
            this.BtnSelect2.Text = "Правые";
            this.BtnSelect2.UseVisualStyleBackColor = true;
            this.BtnSelect2.Click += new System.EventHandler(this.BtnSelect2_Click);
            // 
            // BtnSelect1
            // 
            this.BtnSelect1.Image = global::Robotlon_sw.Properties.Resources.RightRight;
            this.BtnSelect1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSelect1.Location = new System.Drawing.Point(7, 17);
            this.BtnSelect1.Name = "BtnSelect1";
            this.BtnSelect1.Size = new System.Drawing.Size(84, 23);
            this.BtnSelect1.TabIndex = 11;
            this.BtnSelect1.Text = "Левые";
            this.BtnSelect1.UseVisualStyleBackColor = true;
            this.BtnSelect1.Click += new System.EventHandler(this.BtnSelect1_Click);
            // 
            // TxtbName2
            // 
            this.TxtbName2.Location = new System.Drawing.Point(110, 57);
            this.TxtbName2.Name = "TxtbName2";
            this.TxtbName2.Size = new System.Drawing.Size(84, 20);
            this.TxtbName2.TabIndex = 10;
            this.TxtbName2.Text = "Белые";
            this.TxtbName2.TextChanged += new System.EventHandler(this.TxtbName2_TextChanged);
            // 
            // TxtbName1
            // 
            this.TxtbName1.Location = new System.Drawing.Point(7, 57);
            this.TxtbName1.Name = "TxtbName1";
            this.TxtbName1.Size = new System.Drawing.Size(84, 20);
            this.TxtbName1.TabIndex = 8;
            this.TxtbName1.Text = "Красные";
            this.TxtbName1.TextChanged += new System.EventHandler(this.TxtbName1_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.ChbTimerOn);
            this.groupBox9.Controls.Add(this.BtnTmrReset);
            this.groupBox9.Controls.Add(this.TxtbTimer);
            this.groupBox9.Location = new System.Drawing.Point(23, 343);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(299, 99);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Отсчёт времени";
            // 
            // ChbTimerOn
            // 
            this.ChbTimerOn.AutoSize = true;
            this.ChbTimerOn.Location = new System.Drawing.Point(6, 29);
            this.ChbTimerOn.Name = "ChbTimerOn";
            this.ChbTimerOn.Size = new System.Drawing.Size(107, 17);
            this.ChbTimerOn.TabIndex = 4;
            this.ChbTimerOn.Text = "Отсчёт включен";
            this.ChbTimerOn.UseVisualStyleBackColor = true;
            this.ChbTimerOn.CheckedChanged += new System.EventHandler(this.ChbTimerOn_CheckedChanged);
            // 
            // BtnTmrReset
            // 
            this.BtnTmrReset.Location = new System.Drawing.Point(124, 25);
            this.BtnTmrReset.Name = "BtnTmrReset";
            this.BtnTmrReset.Size = new System.Drawing.Size(54, 23);
            this.BtnTmrReset.TabIndex = 3;
            this.BtnTmrReset.Text = "Сброс";
            this.BtnTmrReset.UseVisualStyleBackColor = true;
            this.BtnTmrReset.Click += new System.EventHandler(this.BtnTmrReset_Click);
            // 
            // TxtbTimer
            // 
            this.TxtbTimer.Location = new System.Drawing.Point(6, 68);
            this.TxtbTimer.Name = "TxtbTimer";
            this.TxtbTimer.Size = new System.Drawing.Size(112, 20);
            this.TxtbTimer.TabIndex = 0;
            this.TxtbTimer.Text = "00:00";
            this.TxtbTimer.TextChanged += new System.EventHandler(this.TxtbTimer_TextChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.TxtbState4);
            this.groupBox8.Controls.Add(this.TxtbState3);
            this.groupBox8.Controls.Add(this.TxtbState2);
            this.groupBox8.Controls.Add(this.TxtbState1);
            this.groupBox8.Controls.Add(this.RBtnState4);
            this.groupBox8.Controls.Add(this.RBtnState3);
            this.groupBox8.Controls.Add(this.RBtnState2);
            this.groupBox8.Controls.Add(this.RBtnState1);
            this.groupBox8.Location = new System.Drawing.Point(23, 90);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(299, 145);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Состояние боя";
            // 
            // TxtbState4
            // 
            this.TxtbState4.Location = new System.Drawing.Point(46, 113);
            this.TxtbState4.Name = "TxtbState4";
            this.TxtbState4.Size = new System.Drawing.Size(100, 20);
            this.TxtbState4.TabIndex = 7;
            this.TxtbState4.TextChanged += new System.EventHandler(this.TxtbState4_TextChanged);
            // 
            // TxtbState3
            // 
            this.TxtbState3.Location = new System.Drawing.Point(46, 80);
            this.TxtbState3.Name = "TxtbState3";
            this.TxtbState3.Size = new System.Drawing.Size(100, 20);
            this.TxtbState3.TabIndex = 6;
            this.TxtbState3.Text = "Бой окончен";
            this.TxtbState3.TextChanged += new System.EventHandler(this.TxtbState3_TextChanged);
            // 
            // TxtbState2
            // 
            this.TxtbState2.Location = new System.Drawing.Point(46, 48);
            this.TxtbState2.Name = "TxtbState2";
            this.TxtbState2.Size = new System.Drawing.Size(100, 20);
            this.TxtbState2.TabIndex = 5;
            this.TxtbState2.Text = "Идёт бой";
            this.TxtbState2.TextChanged += new System.EventHandler(this.TxtbState2_TextChanged);
            // 
            // TxtbState1
            // 
            this.TxtbState1.Location = new System.Drawing.Point(46, 16);
            this.TxtbState1.Name = "TxtbState1";
            this.TxtbState1.Size = new System.Drawing.Size(100, 20);
            this.TxtbState1.TabIndex = 4;
            this.TxtbState1.Text = "Приготовиться";
            this.TxtbState1.TextChanged += new System.EventHandler(this.TxtbState1_TextChanged);
            // 
            // RBtnState4
            // 
            this.RBtnState4.AutoSize = true;
            this.RBtnState4.Location = new System.Drawing.Point(19, 116);
            this.RBtnState4.Name = "RBtnState4";
            this.RBtnState4.Size = new System.Drawing.Size(14, 13);
            this.RBtnState4.TabIndex = 3;
            this.RBtnState4.UseVisualStyleBackColor = true;
            this.RBtnState4.Click += new System.EventHandler(this.RBtnState1_Click);
            // 
            // RBtnState3
            // 
            this.RBtnState3.AutoSize = true;
            this.RBtnState3.Location = new System.Drawing.Point(19, 83);
            this.RBtnState3.Name = "RBtnState3";
            this.RBtnState3.Size = new System.Drawing.Size(14, 13);
            this.RBtnState3.TabIndex = 2;
            this.RBtnState3.UseVisualStyleBackColor = true;
            this.RBtnState3.Click += new System.EventHandler(this.RBtnState1_Click);
            // 
            // RBtnState2
            // 
            this.RBtnState2.AutoSize = true;
            this.RBtnState2.Location = new System.Drawing.Point(19, 51);
            this.RBtnState2.Name = "RBtnState2";
            this.RBtnState2.Size = new System.Drawing.Size(14, 13);
            this.RBtnState2.TabIndex = 1;
            this.RBtnState2.UseVisualStyleBackColor = true;
            this.RBtnState2.Click += new System.EventHandler(this.RBtnState1_Click);
            // 
            // RBtnState1
            // 
            this.RBtnState1.AutoSize = true;
            this.RBtnState1.Checked = true;
            this.RBtnState1.Location = new System.Drawing.Point(19, 19);
            this.RBtnState1.Name = "RBtnState1";
            this.RBtnState1.Size = new System.Drawing.Size(14, 13);
            this.RBtnState1.TabIndex = 0;
            this.RBtnState1.TabStop = true;
            this.RBtnState1.UseVisualStyleBackColor = true;
            this.RBtnState1.Click += new System.EventHandler(this.RBtnState1_Click);
            // 
            // TxtbCaption
            // 
            this.TxtbCaption.Location = new System.Drawing.Point(69, 64);
            this.TxtbCaption.Name = "TxtbCaption";
            this.TxtbCaption.Size = new System.Drawing.Size(253, 20);
            this.TxtbCaption.TabIndex = 2;
            this.TxtbCaption.Text = "Шапка";
            this.TxtbCaption.TextChanged += new System.EventHandler(this.TxtbCaption_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 67);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Шапка:";
            // 
            // ChbShowScoreboard
            // 
            this.ChbShowScoreboard.AutoSize = true;
            this.ChbShowScoreboard.Location = new System.Drawing.Point(20, 22);
            this.ChbShowScoreboard.Name = "ChbShowScoreboard";
            this.ChbShowScoreboard.Size = new System.Drawing.Size(107, 17);
            this.ChbShowScoreboard.TabIndex = 0;
            this.ChbShowScoreboard.Text = "Показать табло";
            this.ChbShowScoreboard.UseVisualStyleBackColor = true;
            this.ChbShowScoreboard.CheckedChanged += new System.EventHandler(this.ChbShowScoreboard_CheckedChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox13);
            this.tabPage6.Controls.Add(this.comboBox1);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.groupBox11);
            this.tabPage6.Controls.Add(this.groupBox12);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(325, 466);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Управление боем";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.Honeydew;
            this.groupBox13.Controls.Add(this.BtnStartFight);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Location = new System.Drawing.Point(0, 285);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(319, 83);
            this.groupBox13.TabIndex = 31;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Запуск боя";
            // 
            // BtnStartFight
            // 
            this.BtnStartFight.Location = new System.Drawing.Point(16, 54);
            this.BtnStartFight.Name = "BtnStartFight";
            this.BtnStartFight.Size = new System.Drawing.Size(290, 23);
            this.BtnStartFight.TabIndex = 40;
            this.BtnStartFight.Text = "Сделать это всё";
            this.BtnStartFight.UseVisualStyleBackColor = true;
            this.BtnStartFight.Click += new System.EventHandler(this.BtnStartFight_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(187, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "Выбрать состояние боя \"Идёт бой\"";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(201, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "Сбросить и запустить отсчёт времени";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.dataSet1;
            this.comboBox1.DisplayMember = "TableGroups.ColGroupName";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(68, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(125, 21);
            this.comboBox1.TabIndex = 30;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(17, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 29;
            this.label20.Text = "Группа:";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.AliceBlue;
            this.groupBox11.Controls.Add(this.label27);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Controls.Add(this.CmbSFTurretHits);
            this.groupBox11.Controls.Add(this.CmbSFTargetHits);
            this.groupBox11.Controls.Add(this.CmbSFShotsTanks);
            this.groupBox11.Controls.Add(this.CmbSFTankHits);
            this.groupBox11.Controls.Add(this.BtnPrepare);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Controls.Add(this.label24);
            this.groupBox11.Controls.Add(this.label23);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Controls.Add(this.label19);
            this.groupBox11.Location = new System.Drawing.Point(0, 65);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(319, 214);
            this.groupBox11.TabIndex = 27;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Приготовиться";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(13, 138);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(218, 13);
            this.label27.TabIndex = 38;
            this.label27.Text = "Выбрать состояние боя \"Приготовиться\"";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(231, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "Сбросить количество поражённых мишеней";
            // 
            // CmbSFTurretHits
            // 
            this.CmbSFTurretHits.FormattingEnabled = true;
            this.CmbSFTurretHits.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSFTurretHits.Location = new System.Drawing.Point(103, 96);
            this.CmbSFTurretHits.Name = "CmbSFTurretHits";
            this.CmbSFTurretHits.Size = new System.Drawing.Size(52, 21);
            this.CmbSFTurretHits.TabIndex = 36;
            this.CmbSFTurretHits.Text = "4";
            // 
            // CmbSFTargetHits
            // 
            this.CmbSFTargetHits.FormattingEnabled = true;
            this.CmbSFTargetHits.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSFTargetHits.Location = new System.Drawing.Point(103, 72);
            this.CmbSFTargetHits.Name = "CmbSFTargetHits";
            this.CmbSFTargetHits.Size = new System.Drawing.Size(52, 21);
            this.CmbSFTargetHits.TabIndex = 35;
            this.CmbSFTargetHits.Text = "4";
            // 
            // CmbSFShotsTanks
            // 
            this.CmbSFShotsTanks.FormattingEnabled = true;
            this.CmbSFShotsTanks.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSFShotsTanks.Location = new System.Drawing.Point(241, 49);
            this.CmbSFShotsTanks.Name = "CmbSFShotsTanks";
            this.CmbSFShotsTanks.Size = new System.Drawing.Size(64, 21);
            this.CmbSFShotsTanks.TabIndex = 34;
            this.CmbSFShotsTanks.Text = "18";
            // 
            // CmbSFTankHits
            // 
            this.CmbSFTankHits.FormattingEnabled = true;
            this.CmbSFTankHits.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "5",
            "7",
            "10",
            "15"});
            this.CmbSFTankHits.Location = new System.Drawing.Point(103, 49);
            this.CmbSFTankHits.Name = "CmbSFTankHits";
            this.CmbSFTankHits.Size = new System.Drawing.Size(52, 21);
            this.CmbSFTankHits.TabIndex = 33;
            this.CmbSFTankHits.Text = "4";
            // 
            // BtnPrepare
            // 
            this.BtnPrepare.Location = new System.Drawing.Point(16, 185);
            this.BtnPrepare.Name = "BtnPrepare";
            this.BtnPrepare.Size = new System.Drawing.Size(290, 23);
            this.BtnPrepare.TabIndex = 32;
            this.BtnPrepare.Text = "Сделать это всё";
            this.BtnPrepare.UseVisualStyleBackColor = true;
            this.BtnPrepare.Click += new System.EventHandler(this.BtnPrepare_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 156);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(249, 13);
            this.label25.TabIndex = 31;
            this.label25.Text = "Остановить отсчёт времени и сбросить таймер";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(173, 52);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 13);
            this.label24.TabIndex = 30;
            this.label24.Text = "Выстрелы:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 99);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "Хиты турелей:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 75);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Хиты мишеней:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Задать:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Хиты танков:";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.MistyRose;
            this.groupBox12.Controls.Add(this.BtnStopFight);
            this.groupBox12.Controls.Add(this.label31);
            this.groupBox12.Controls.Add(this.label30);
            this.groupBox12.Location = new System.Drawing.Point(0, 374);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(319, 92);
            this.groupBox12.TabIndex = 28;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Остановка боя";
            // 
            // BtnStopFight
            // 
            this.BtnStopFight.Location = new System.Drawing.Point(16, 57);
            this.BtnStopFight.Name = "BtnStopFight";
            this.BtnStopFight.Size = new System.Drawing.Size(290, 23);
            this.BtnStopFight.TabIndex = 42;
            this.BtnStopFight.Text = "Сделать это всё";
            this.BtnStopFight.UseVisualStyleBackColor = true;
            this.BtnStopFight.Click += new System.EventHandler(this.BtnStopFight_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 39);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(150, 13);
            this.label31.TabIndex = 41;
            this.label31.Text = "Остановить отсчёт времени";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(165, 13);
            this.label30.TabIndex = 40;
            this.label30.Text = "Выбрать состояние боя \"Стоп\"";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1139, 498);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Управление группами";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.DgvDevInGroup);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(254, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(882, 492);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Список устройств в группе";
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label14.Location = new System.Drawing.Point(3, 443);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(876, 46);
            this.label14.TabIndex = 1;
            this.label14.Text = "Левой кнопкой мыши выделите устройства, а правой кнопкой перетащите их в нужную г" +
    "руппу";
            // 
            // DgvDevInGroup
            // 
            this.DgvDevInGroup.AllowDrop = true;
            this.DgvDevInGroup.AllowUserToAddRows = false;
            this.DgvDevInGroup.AllowUserToDeleteRows = false;
            this.DgvDevInGroup.AllowUserToResizeRows = false;
            this.DgvDevInGroup.AutoGenerateColumns = false;
            this.DgvDevInGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDevInGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn2,
            this.типDataGridViewTextBoxColumn2,
            this.названиеDataGridViewTextBoxColumn2,
            this.группаDataGridViewTextBoxColumn2});
            this.DgvDevInGroup.DataMember = "TableDevices";
            this.DgvDevInGroup.DataSource = this.dataSet1;
            this.DgvDevInGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvDevInGroup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DgvDevInGroup.Location = new System.Drawing.Point(3, 16);
            this.DgvDevInGroup.Name = "DgvDevInGroup";
            this.DgvDevInGroup.ReadOnly = true;
            this.DgvDevInGroup.RowHeadersVisible = false;
            this.DgvDevInGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvDevInGroup.Size = new System.Drawing.Size(876, 473);
            this.DgvDevInGroup.TabIndex = 0;
            this.DgvDevInGroup.VirtualMode = true;
            this.DgvDevInGroup.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DgvDevices_CellPainting);
            this.DgvDevInGroup.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DgvDevInGroup_MouseMove);
            // 
            // iDDataGridViewTextBoxColumn2
            // 
            this.iDDataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn2.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn2.Name = "iDDataGridViewTextBoxColumn2";
            this.iDDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // типDataGridViewTextBoxColumn2
            // 
            this.типDataGridViewTextBoxColumn2.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn2.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn2.Name = "типDataGridViewTextBoxColumn2";
            this.типDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // названиеDataGridViewTextBoxColumn2
            // 
            this.названиеDataGridViewTextBoxColumn2.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn2.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn2.Name = "названиеDataGridViewTextBoxColumn2";
            this.названиеDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // группаDataGridViewTextBoxColumn2
            // 
            this.группаDataGridViewTextBoxColumn2.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn2.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn2.Name = "группаDataGridViewTextBoxColumn2";
            this.группаDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DgvGrpList);
            this.groupBox5.Controls.Add(this.panel2);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(251, 492);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Список групп";
            // 
            // DgvGrpList
            // 
            this.DgvGrpList.AllowDrop = true;
            this.DgvGrpList.AllowUserToAddRows = false;
            this.DgvGrpList.AllowUserToDeleteRows = false;
            this.DgvGrpList.AllowUserToResizeRows = false;
            this.DgvGrpList.AutoGenerateColumns = false;
            this.DgvGrpList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DgvGrpList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvGrpList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGroupNameDataGridViewTextBoxColumn});
            this.DgvGrpList.DataMember = "TableGroups";
            this.DgvGrpList.DataSource = this.dataSet1;
            this.DgvGrpList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvGrpList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.DgvGrpList.Location = new System.Drawing.Point(3, 16);
            this.DgvGrpList.Name = "DgvGrpList";
            this.DgvGrpList.RowHeadersVisible = false;
            this.DgvGrpList.Size = new System.Drawing.Size(245, 427);
            this.DgvGrpList.TabIndex = 1;
            this.DgvGrpList.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvGrpList_CellBeginEdit);
            this.DgvGrpList.CurrentCellChanged += new System.EventHandler(this.DgvGrpList_CurrentCellChanged);
            this.DgvGrpList.DragDrop += new System.Windows.Forms.DragEventHandler(this.DgvGrpList_DragDrop);
            this.DgvGrpList.DragOver += new System.Windows.Forms.DragEventHandler(this.DgvGrpList_DragOver);
            // 
            // colGroupNameDataGridViewTextBoxColumn
            // 
            this.colGroupNameDataGridViewTextBoxColumn.DataPropertyName = "ColGroupName";
            this.colGroupNameDataGridViewTextBoxColumn.HeaderText = "Группа";
            this.colGroupNameDataGridViewTextBoxColumn.Name = "colGroupNameDataGridViewTextBoxColumn";
            this.colGroupNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnDeleteGroup);
            this.panel2.Controls.Add(this.BtnAddGroup);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 443);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(245, 46);
            this.panel2.TabIndex = 0;
            // 
            // BtnDeleteGroup
            // 
            this.BtnDeleteGroup.Location = new System.Drawing.Point(123, 20);
            this.BtnDeleteGroup.Name = "BtnDeleteGroup";
            this.BtnDeleteGroup.Size = new System.Drawing.Size(75, 23);
            this.BtnDeleteGroup.TabIndex = 1;
            this.BtnDeleteGroup.Text = "Удалить";
            this.BtnDeleteGroup.UseVisualStyleBackColor = true;
            this.BtnDeleteGroup.Click += new System.EventHandler(this.BtnDeleteGroup_Click);
            // 
            // BtnAddGroup
            // 
            this.BtnAddGroup.Location = new System.Drawing.Point(42, 20);
            this.BtnAddGroup.Name = "BtnAddGroup";
            this.BtnAddGroup.Size = new System.Drawing.Size(75, 23);
            this.BtnAddGroup.TabIndex = 0;
            this.BtnAddGroup.Text = "Добавить";
            this.BtnAddGroup.UseVisualStyleBackColor = true;
            this.BtnAddGroup.Click += new System.EventHandler(this.BtnAddGroup_Click);
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            // 
            // типDataGridViewTextBoxColumn1
            // 
            this.типDataGridViewTextBoxColumn1.DataPropertyName = "Тип";
            this.типDataGridViewTextBoxColumn1.HeaderText = "Тип";
            this.типDataGridViewTextBoxColumn1.Name = "типDataGridViewTextBoxColumn1";
            // 
            // названиеDataGridViewTextBoxColumn1
            // 
            this.названиеDataGridViewTextBoxColumn1.DataPropertyName = "Название";
            this.названиеDataGridViewTextBoxColumn1.HeaderText = "Название";
            this.названиеDataGridViewTextBoxColumn1.Name = "названиеDataGridViewTextBoxColumn1";
            // 
            // группаDataGridViewTextBoxColumn1
            // 
            this.группаDataGridViewTextBoxColumn1.DataPropertyName = "Группа";
            this.группаDataGridViewTextBoxColumn1.HeaderText = "Группа";
            this.группаDataGridViewTextBoxColumn1.Name = "группаDataGridViewTextBoxColumn1";
            // 
            // Хиты
            // 
            this.Хиты.DataPropertyName = "Хиты";
            this.Хиты.HeaderText = "Хиты";
            this.Хиты.Name = "Хиты";
            this.Хиты.Visible = false;
            // 
            // Выстрелы
            // 
            this.Выстрелы.DataPropertyName = "Выстрелы";
            this.Выстрелы.HeaderText = "Выстрелы";
            this.Выстрелы.Name = "Выстрелы";
            this.Выстрелы.Visible = false;
            // 
            // Состояние
            // 
            this.Состояние.DataPropertyName = "Состояние";
            this.Состояние.HeaderText = "Состояние";
            this.Состояние.Name = "Состояние";
            this.Состояние.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "discon2.bmp");
            this.imageList1.Images.SetKeyName(1, "connect2.bmp");
            // 
            // TimerScore
            // 
            this.TimerScore.Interval = 1000;
            this.TimerScore.Tick += new System.EventHandler(this.TimerScore_Tick);
            // 
            // ChbShowRight
            // 
            this.ChbShowRight.AutoSize = true;
            this.ChbShowRight.Checked = true;
            this.ChbShowRight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChbShowRight.Location = new System.Drawing.Point(147, 22);
            this.ChbShowRight.Name = "ChbShowRight";
            this.ChbShowRight.Size = new System.Drawing.Size(165, 17);
            this.ChbShowRight.TabIndex = 13;
            this.ChbShowRight.Text = "Показать правую половину";
            this.ChbShowRight.UseVisualStyleBackColor = true;
            this.ChbShowRight.CheckedChanged += new System.EventHandler(this.ChbShowRight_CheckedChanged);
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 546);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Mainform";
            this.Text = "Robotlon Control";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Mainform_FormClosed);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDevTypes)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvDevInGroup)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvGrpList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView DgvDevices;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable TableDevices;
        private System.Data.DataTable TableGroups;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView DgvGrpList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button BtnDeleteGroup;
        private System.Windows.Forms.Button BtnAddGroup;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView DgvDevInGroup;
        private System.Data.DataColumn ColID;
        private System.Data.DataColumn ColType;
        private System.Data.DataColumn ColName;
        private System.Data.DataColumn ColGroup;
        private System.Data.DataColumn ColHitCnt;
        private System.Data.DataColumn ColShotCnt;
        private System.Data.DataColumn ColTargetsHitCnt;
        private System.Data.DataColumn ColState;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGroupNameDataGridViewTextBoxColumn;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataTable TableDevStates;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataTable TableDevTypes;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Хиты;
        private System.Windows.Forms.DataGridViewTextBoxColumn Выстрелы;
        private System.Windows.Forms.DataGridViewTextBoxColumn Состояние;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ImageList imageList1;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn9;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button BtnSendState;
        private System.Windows.Forms.ComboBox CmbGroup;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnSetShotCnt;
        private System.Windows.Forms.ComboBox CmbShotCnt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button BtnSetHitCnt;
        private System.Windows.Forms.ComboBox CmbHitCnt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CmbHitRecipient;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnResetTargetCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CmbDevState;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox CmbSingleGroupFilter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button BtnSingleResetTargetCount;
        private System.Windows.Forms.Button BtnSingleSendState;
        private System.Windows.Forms.ComboBox CmbSingleState;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button BtnSingleSetShotCnt;
        private System.Windows.Forms.ComboBox CmbSingleShotCnt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnSingleSetHitCnt;
        private System.Windows.Forms.ComboBox CmbSingleHitCnt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CmbIRPwr;
        private System.Windows.Forms.Button BtnSendRepairTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox CmbRepairTime;
        private System.Windows.Forms.Button BtnSendReloadTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox CmbReloadTime;
        private System.Windows.Forms.Button BtnSendIRDamage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox CmbIRDamage;
        private System.Windows.Forms.Button BtnSendIRPwr;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn типDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn названиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn группаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn хитыDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn выстрелыDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn мишениDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn состояниеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCommState;
        private System.Windows.Forms.CheckBox ChbShowScoreboard;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox TxtbState4;
        private System.Windows.Forms.TextBox TxtbState3;
        private System.Windows.Forms.TextBox TxtbState2;
        private System.Windows.Forms.TextBox TxtbState1;
        private System.Windows.Forms.RadioButton RBtnState4;
        private System.Windows.Forms.RadioButton RBtnState3;
        private System.Windows.Forms.RadioButton RBtnState2;
        private System.Windows.Forms.RadioButton RBtnState1;
        private System.Windows.Forms.TextBox TxtbCaption;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox TxtbName2;
        private System.Windows.Forms.TextBox TxtbName1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox TxtbTimer;
        private System.Windows.Forms.Timer TimerScore;
        private System.Windows.Forms.Button BtnTmrReset;
        private System.Windows.Forms.Button BtnSelect1;
        private System.Windows.Forms.Button BtnSelect2;
        private System.Windows.Forms.CheckBox ChbTimerOn;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button BtnStartFight;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox CmbSFTurretHits;
        private System.Windows.Forms.ComboBox CmbSFTargetHits;
        private System.Windows.Forms.ComboBox CmbSFShotsTanks;
        private System.Windows.Forms.ComboBox CmbSFTankHits;
        private System.Windows.Forms.Button BtnPrepare;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button BtnStopFight;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox ChbShowRight;
    }
}

