﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.IO.Ports;

namespace Robotlon_sw {
    public partial class Mainform : Form {
        #region ================== Constants and variables ===================
        // Column indxs
        const int DEV_CNT = 32;
        // Devices dgv
        const int COL_ID = 0;
        const int COL_TYPE = 1;
        const int COL_NAME = 2;
        const int COL_GROUP = 3;
        const int COL_HITS = 4;
        const int COL_SHOTS = 5;
        const int COL_TARGETS = 6;
        const int COL_DEVSTATE = 7;
        const int COL_COMM_ST = 8;
        // Groups table
        const int COL_GRPS_GRP_NAME = 0;
        const int COL_GRPS_GRP_ID = 1;

        // Communication states
        const string cstInSync = "cstInSync";
        const string cstAwaitingReply = "sctAwaitingReply";
        const string cstTimeout = "cstTimeout";
        Color ClrInSync = SystemColors.Window;
        Color ClrAwaitingReply = Color.PapayaWhip;
        Color ClrTimeout = Color.Red;

        // ==== Strings ====
        const string TABLE_DEVICES_FILENAME = "TableDevices.xml";
        const string GROUP_NAME_ALL = "Все";
        // Device states
        const string DEVST_PREPARE = "Приготовиться";
        const string DEVST_OPERATIONAL = "Исправен";
        const string DEVST_DAMAGED = "Повреждён";
        const string DEVST_RELOAD = "Перезарядка";
        const string DEVST_DESTROYED = "Уничтожен";
        const string DEVST_STOP = "Стоп";
        // Device types
        const string DEVT_TANK = "Танк";
        const string DEVT_TARGET = "Мишень";
        const string DEVT_TURRET = "Турель";

        // Pipes communication class
        Pipes_t Pipes = new Pipes_t(DEV_CNT);

        // Scoreboard
        Scoreboard Scrbrd;
        #endregion

        #region ================== Init / Deinit ===================
        public Mainform() {
            InitializeComponent();

            // Init groups
            TableGroups.Rows.Add("Unknown", "-2");
            TableGroups.Rows.Add(GROUP_NAME_ALL, "-1");
            TableGroups.Rows.Add("Отладка", "0");
            TableGroups.Rows.Add("Боевая", "1");

            // Tables 
            TableDevStates.Rows.Add(DEVST_PREPARE, "0");
            TableDevStates.Rows.Add(DEVST_OPERATIONAL, "1");
            TableDevStates.Rows.Add(DEVST_DAMAGED, "2");
            TableDevStates.Rows.Add(DEVST_RELOAD, "3");
            TableDevStates.Rows.Add(DEVST_DESTROYED, "4");
            TableDevStates.Rows.Add(DEVST_STOP, "5");

            TableDevTypes.Rows.Add(DEVT_TANK, "0");
            TableDevTypes.Rows.Add(DEVT_TARGET, "1");
            TableDevTypes.Rows.Add(DEVT_TURRET, "2");

            // Setup devices dataset
            TableDevices.Columns[COL_GROUP].DefaultValue = "Unknown";
            try {
                System.Data.XmlReadMode rslt = TableDevices.ReadXml(TABLE_DEVICES_FILENAME);
            }
            catch {
                // Load failed, fill with default values
                TableDevices.Clear();
                // Fill dataset
                string DevSt = TableDevStates.Rows[1][0].ToString();
                for(int i = 1; i <= DEV_CNT; i++) {
                    TableDevices.Rows.Add(i, "Unknown", "-", "Unknown", "4", "18", "0", DevSt, cstInSync);
                }
            }

            // OnChange event to redraw Scoreboard
            TableDevices.RowChanged += TableDevices_RowChanged;

            // Setup datagridviews to be filterable
            DgvDevices.DataSource = null;   // Some weird magic to avoid exception on next line
            DgvDevices.DataSource = TableDevices.DefaultView;
            DgvDevInGroup.DataSource = null;
            DgvDevInGroup.DataSource = TableDevices.DefaultView;

            // Comboboxes
            CmbGroup.SelectedIndex = 1;
            CmbIRDamage.SelectedIndex = 1;

            // Data management
            Pipes_OnConnectDisconnect(false, "");
            Pipes.OnNewData += Pipes_OnNewData;
            Pipes.OnConnectDisconnect += Pipes_OnConnectDisconnect;
        }

        private void Mainform_FormClosed(object sender, FormClosedEventArgs e) {
            // Save data to file
            try {
                TableDevices.WriteXml(TABLE_DEVICES_FILENAME);
            }
            catch { }
        }
        #endregion

        #region ================= Indication ================
        private void Pipes_OnConnectDisconnect(bool IsConnected, string Description) {
            if(IsConnected) {
                StatusLabel.Image = imageList1.Images[1];
                StatusLabel.Text = "Передатчик подключен на " + Description;
            }
            else {
                StatusLabel.Image = imageList1.Images[0];
                StatusLabel.Text = "Передатчик не подключен";
                if(Description != "") StatusLabel.Text += ": " + Description;
            }
        }

        // Change back color of cells depending on communication state
        private void DgvDevices_CellPainting(object sender, DataGridViewCellPaintingEventArgs e) {
            if(e.ColumnIndex != COL_ID || e.RowIndex < 0) return;
            string CommSt = DgvDevices.Rows[e.RowIndex].Cells[COL_COMM_ST].Value.ToString();
            Color Clr;
            if(CommSt.Equals(cstInSync)) Clr = ClrInSync;
            else if(CommSt.Equals(cstAwaitingReply)) Clr = ClrAwaitingReply;
            else Clr = ClrTimeout;
            e.CellStyle.BackColor = Clr;
        }
        #endregion

        #region ==== Checkers ====
        private bool CheckNumericCombo(ComboBox cmb) {
            if(!int.TryParse(cmb.Text, out int Value)) {
                cmb.BackColor = Color.Red;
                return false;
            }
            else {
                cmb.BackColor = SystemColors.Window;
                return true;
            }
        }

        private bool CheckNumericCombo(ComboBox cmb, int MinValue, int MaxValue) {
            if(int.TryParse(cmb.Text, out int Value)) {
                if(Value >= MinValue && Value <= MaxValue) {
                    cmb.BackColor = SystemColors.Window;
                    return true;
                }
            }
            cmb.BackColor = Color.Red;
            return false;
        }

        private void CheckOnTextChanged(object sender, EventArgs e) {
            CheckNumericCombo(sender as ComboBox);
        }
        #endregion

        #region ================= Game control =================
        public delegate void ParamSender(string ID, string Data);   // Function pointer
        
        private void SetGroupState(string NewState) {
            string DevStID = TableDevStates.Rows.Find(NewState)[1].ToString();
            SetNewValue(NewState, DevStID, COL_DEVSTATE, Pipes.SendState);
        }

        #region ==== Group ====
        // Filter devices by group
        private void CmbGroup_TextChanged(object sender, EventArgs e) {
            string SelectedGrp = CmbGroup.Text;
            string fmt;
            if(SelectedGrp.Equals(GROUP_NAME_ALL)) fmt = string.Empty;  // Make all visible if "all" selected
            else fmt = string.Format("[{0}] LIKE '%{1}%'", TableDevices.Columns[COL_GROUP].ColumnName, SelectedGrp);
            TableDevices.DefaultView.RowFilter = fmt;
        }

        private void SetNewValue(string NewValueToShow, string NewValueToSend, int Column, ParamSender ParSender, string DevTypeFilter = "") {
            DgvDevices.Enabled = false;
            foreach(DataGridViewRow row in DgvDevices.Rows) {
                // Filter device by type if required
                if(DevTypeFilter != "") {
                    if(!DevTypeFilter.Equals(row.Cells[COL_TYPE].Value.ToString())) continue;
                }
                //string OldValue = row.Cells[Column].Value.ToString();
                //if(OldValue != NewValue) {
                    row.Cells[Column].Value = NewValueToShow;
                    row.Cells[COL_COMM_ST].Value = cstAwaitingReply;
                    // Send data
                    ParSender(row.Cells[COL_ID].Value.ToString(), NewValueToSend);
                //}
            }
            DgvDevices.Enabled = true;
        }

        // Set hit cnt
        private void BtnSetHitCnt_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbHitCnt)) return;
            SetNewValue(CmbHitCnt.Text, CmbHitCnt.Text, COL_HITS, Pipes.SendHitCnt, CmbHitRecipient.Text);
        }

        // Set shot cnt
        private void BtnSetShotCnt_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbShotCnt)) return;
            SetNewValue(CmbShotCnt.Text, CmbShotCnt.Text, COL_SHOTS, Pipes.SendShotCnt, DEVT_TANK);  // Shot cnt is only for tanks
        }
        
        // Set device state
        private void BtnSendState_Click(object sender, EventArgs e)  {
            SetGroupState(CmbDevState.Text);
        }

        // Reset target cnt
        private void BtnResetTargetCount_Click(object sender, EventArgs e) {
            foreach(DataGridViewRow row in DgvDevices.Rows) {
                row.Cells[COL_TARGETS].Value = 0;
            }
        }
        #endregion // Group

        #region ==== Single ====
        private void SetSingleNewValue(string NewValueToShow, string NewValueToSend, int Column, ParamSender ParSender) {
            if(DgvDevices.SelectedRows.Count == 0) return;      // If nothing selected
            DataGridViewRow row = DgvDevices.SelectedRows[0];
            //string OldValue = row.Cells[Column].Value.ToString();
            //if(OldValue != NewValue) {
                row.Cells[Column].Value = NewValueToShow;
                row.Cells[COL_COMM_ST].Value = cstAwaitingReply;
                // Send data
                ParSender(row.Cells[COL_ID].Value.ToString(), NewValueToSend);
            //}
        }

        // Set hit cnt
        private void BtnSingleSetHitCnt_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbSingleHitCnt)) return;  // If bad value
            SetSingleNewValue(CmbSingleHitCnt.Text, CmbSingleHitCnt.Text, COL_HITS, Pipes.SendHitCnt);
        }

        private void BtnSingleSetShotCnt_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbSingleShotCnt)) return;  // If bad value
            SetSingleNewValue(CmbSingleShotCnt.Text, CmbSingleShotCnt.Text, COL_SHOTS, Pipes.SendShotCnt);
        }

        // Set device state
        private void BtnSingleSendState_Click(object sender, EventArgs e) {
            string DevSt = CmbDevState.Text;
            string DevStID = TableDevStates.Rows.Find(DevSt)[1].ToString();
            SetSingleNewValue(DevSt, DevStID, COL_DEVSTATE, Pipes.SendState);
        }

        private void BtnSingleResetTargetCount_Click(object sender, EventArgs e) {
            if(DgvDevices.SelectedRows.Count == 0) return;      // If nothing selected
            DataGridViewRow row = DgvDevices.SelectedRows[0];
            row.Cells[COL_TARGETS].Value = 0;
        }
        #endregion // Single

        #region ======== Fighting Control ========
        private void BtnPrepare_Click(object sender, EventArgs e) {
            // Check comboboxes
            if(!CheckNumericCombo(CmbSFTankHits, 1, 255)) return;
            if(!CheckNumericCombo(CmbSFTargetHits, 1, 255)) return;
            if(!CheckNumericCombo(CmbSFTurretHits, 1, 255)) return;
            if(!CheckNumericCombo(CmbSFShotsTanks, 1, 255)) return;
            // Set hit cnt
            SetNewValue(CmbSFTankHits.Text, CmbSFTankHits.Text, COL_HITS, Pipes.SendHitCnt, DEVT_TANK);
            SetNewValue(CmbSFTargetHits.Text, CmbSFTargetHits.Text, COL_HITS, Pipes.SendHitCnt, DEVT_TARGET);
            SetNewValue(CmbSFTurretHits.Text, CmbSFTurretHits.Text, COL_HITS, Pipes.SendHitCnt, DEVT_TURRET);
            // Set shot cnt
            SetNewValue(CmbSFShotsTanks.Text, CmbSFShotsTanks.Text, COL_SHOTS, Pipes.SendShotCnt, DEVT_TANK);
            // Reset target cnt
            BtnResetTargetCount_Click(null, null);
            // Setup FightState
            RBtnState1.Checked = true;
            RBtnState1_Click(null, null);
            SetGroupState(DEVST_PREPARE);
            // Stop timer and reset it
            ChbTimerOn.Checked = false;
            BtnTmrReset_Click(null, null);
        }

        private void BtnStartFight_Click(object sender, EventArgs e) {
            // Setup FightState
            RBtnState2.Checked = true;
            RBtnState1_Click(null, null);
            SetGroupState(DEVST_OPERATIONAL);
            // Reset timer and start it
            BtnTmrReset_Click(null, null);
            ChbTimerOn.Checked = true;
        }

        private void BtnStopFight_Click(object sender, EventArgs e) {
            // Setup FightState
            RBtnState3.Checked = true;
            RBtnState1_Click(null, null);
            SetGroupState(DEVST_STOP);
            // Stop timer 
            ChbTimerOn.Checked = false;
        }
        #endregion

        #endregion

        #region ================= Settings ====================
        private void BtnSendIRPwr_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbIRPwr, 0, 255)) return;    // Check value
            string ID = DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString();
            Pipes.SendParam(ID, 30, CmbIRPwr.Text);
        }

        private void BtnSendIRDamage_Click(object sender, EventArgs e) {
            string ID = DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString();
            Pipes.SendParam(ID, 31, CmbIRDamage.Text);
        }

        private void BtnSendReloadTime_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbReloadTime, 0, 255)) return;    // Check value
            string ID = DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString();
            Pipes.SendParam(ID, 32, CmbReloadTime.Text);
        }

        private void BtnSendRepairTime_Click(object sender, EventArgs e) {
            if(!CheckNumericCombo(CmbRepairTime, 0, 255)) return;    // Check value
            string ID = DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString();
            Pipes.SendParam(ID, 33, CmbRepairTime.Text);
        }
        #endregion

        #region ================= Group control =================
        // Filter devices depending on selected group
        private void DgvGrpList_CurrentCellChanged(object sender, EventArgs e) {
            if(DgvGrpList.CurrentCell is null) return;
            string SelectedGrp = DgvGrpList.CurrentCell.Value.ToString();
            string fmt;
            if(SelectedGrp.Equals(GROUP_NAME_ALL)) fmt = string.Empty;  // Make all visible if "all" selected
            else fmt = string.Format("[{0}] LIKE '%{1}%'", TableDevices.Columns[COL_GROUP].ColumnName, SelectedGrp);
            TableDevices.DefaultView.RowFilter = fmt;
        }

        // Select rows to drag
        private void DgvDevInGroup_MouseMove(object sender, MouseEventArgs e) {
            if((e.Button & MouseButtons.Right) == MouseButtons.Right) {
                DgvDevInGroup.DoDragDrop(DgvDevInGroup.SelectedRows, DragDropEffects.Copy);
            }
        }

        // Check if allow dropping 
        private void DgvGrpList_DragOver(object sender, DragEventArgs e) {
            Point Pnt = DgvGrpList.PointToClient(new Point(e.X, e.Y));
            int RowIndx = DgvGrpList.HitTest(Pnt.X, Pnt.Y).RowIndex;
            if(RowIndx > 1) e.Effect = DragDropEffects.Copy; // Allow drop over not "ALL" groups
            else e.Effect = DragDropEffects.None;
        }

        // When drop occured
        private void DgvGrpList_DragDrop(object sender, DragEventArgs e) {
            if(e.Effect != DragDropEffects.Copy) return;
            if(e.Data.GetDataPresent(typeof(DataGridViewSelectedRowCollection))) {
                // Get group to drop to
                Point Pnt = DgvGrpList.PointToClient(new Point(e.X, e.Y));
                int RowIndx = DgvGrpList.HitTest(Pnt.X, Pnt.Y).RowIndex;
                string GrpName = TableGroups.Rows[RowIndx][COL_GRPS_GRP_NAME].ToString();
                string GrpID = TableGroups.Rows[RowIndx][COL_GRPS_GRP_ID].ToString();
                if(GrpID.Equals("-1") || GrpID.Equals("-2")) {
                    MessageBox.Show("Этой группы на самом деле не существует, переместить устройство в неё нельзя.", "Перемещение в группу", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                // Get devices
                DataGridViewSelectedRowCollection rows = e.Data.GetData(typeof(DataGridViewSelectedRowCollection)) as DataGridViewSelectedRowCollection;
                foreach(DataGridViewRow row in rows) {
                    string sID = row.Cells[0].Value.ToString();
                    int ID = Convert.ToInt32(sID);
                    RowIndx = ID - 1;
                    // Highlight changed 
                    string OldGroup = TableDevices.Rows[RowIndx][COL_GROUP].ToString();
                    if(OldGroup != GrpName) {
                        TableDevices.Rows[RowIndx][COL_GROUP] = GrpName;
                        TableDevices.Rows[RowIndx][COL_COMM_ST] = cstAwaitingReply;
                        Pipes.SendNewGroup(sID, GrpID);
                    }
                }
            } // if data present
        }

        // Group editing
        private void DgvGrpList_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) {
            string GroupName = DgvGrpList[e.ColumnIndex, e.RowIndex].Value.ToString();
            if(GroupName.Equals(GROUP_NAME_ALL) || GroupName.Equals("Unknown")) {
                e.Cancel = true;
                MessageBox.Show("Этой группы на самом деле не существует, и её нельзя переименовать", "Переименование группы", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 
        }

        // Add a group
        private void BtnAddGroup_Click(object sender, EventArgs e) {
            // Get new group ID
            int Indx = TableGroups.Rows.Count - 1;
            int LastID = Convert.ToInt32(TableGroups.Rows[Indx][1]);
            int NewID = LastID + 1;
            string SNewID = NewID.ToString();
            TableGroups.Rows.Add("Группа" + SNewID, SNewID);
        }

        private void BtnDeleteGroup_Click(object sender, EventArgs e) {
            if(DgvGrpList.CurrentCell is null) return;
            string GroupName = DgvGrpList.CurrentCell.Value.ToString();
            if(GroupName.Equals(GROUP_NAME_ALL) || GroupName.Equals("Unknown")) {
                MessageBox.Show("Этой группы на самом деле не существует, и её нельзя удалить", "Удаление группы", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else {
                if(TableGroups.Rows.Count < 4) {
                    MessageBox.Show("Осталась последняя группа, её нельзя удалять", "Удаление группы", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                // Remove the row
                DgvGrpList.Rows.Remove(DgvGrpList.CurrentCell.OwningRow);
                // Reenumerate all groups
                int Cnt = TableGroups.Rows.Count;
                for(int i = 0; i < Cnt; i++) {
                    TableGroups.Rows[i][COL_GRPS_GRP_ID] = i-1;
                }
            }
        }
        #endregion

        #region ================= On new data reception =================
        private void Pipes_OnNewData(DevInfo_t Info) {
            System.Data.DataRow row = TableDevices.Rows[Info.ID - 1];
            DgvDevices.CurrentCell = null;
            // Update device communication state
            if(Info.MsgType.Equals("0") || Info.MsgType.Equals("2")) { // Reply OK or polling ok
                if(Info.QIsEmpty) row[COL_COMM_ST] = cstInSync; // Msg Q is empty
                else row[COL_COMM_ST] = cstAwaitingReply;       // Msg Q is not empty
            }
            else if(Info.MsgType.Equals("1") || Info.MsgType.Equals("3")) { // Reply FAILURE => timeout occured, or general timeout
                row[COL_COMM_ST] = cstTimeout;
                return; // Nothing to do here
            }
            
            // Calculate target count
            if(Info.Type == "1" || Info.Type == "2") {  // Target or turret
                int OldHitCnt = int.Parse(row[COL_HITS].ToString());
                int NewHitCnt = int.Parse(Info.HitCnt);
                if(NewHitCnt < OldHitCnt && NewHitCnt == 0) { // Was destroyed by hit
                    int AttackerID = int.Parse(Info.LastAttacker);
                    System.Data.DataRow rowAtt = TableDevices.Rows[AttackerID - 1];
                    int TargetCnt = int.Parse(rowAtt[COL_TARGETS].ToString());
                    TargetCnt++;
                    rowAtt[COL_TARGETS] = TargetCnt;
                }
            }

            // ==== Update table with received data ====
            // Type
            try {
                string S = TableDevTypes.Rows.Find(Info.Type)[0].ToString();
                if(row[COL_TYPE].ToString() != S) row[COL_TYPE] = S;
            }
            catch {
                row[COL_TYPE] = "Танк";
            }
            // Group 
            try {
                string S = TableGroups.Rows.Find(Info.Group)[0].ToString();
                if(row[COL_GROUP].ToString() != S) row[COL_GROUP] = S;
            }
            catch {
                row[COL_GROUP] = "Unknown";
            }

            // Hits  & shots
            if(row[COL_HITS].ToString() != Info.HitCnt) row[COL_HITS] = Info.HitCnt;
            if(row[COL_SHOTS].ToString() != Info.ShotCnt) row[COL_SHOTS] = Info.ShotCnt;
            
            // Get device state 
            string fmt;
            System.Data.DataRow[] rows;
            fmt = string.Format("[{0}] LIKE '%{1}%'", TableDevStates.Columns[1].ColumnName, Info.State);
            rows = TableDevStates.Select(fmt);  // SELECT FROM TableDevStates WHERE DevStateID == Info.State
            row[COL_DEVSTATE] = rows[0][0];     // and put corresponding name in the table
        }
        #endregion

        #region ================= Scoreboard =================
        private int SelectedID1 = 1, SelectedID2 = 2;

        private void DisplayDevDataOnScorebrd() {
            // === ID1 ===
            System.Data.DataRow row = TableDevices.Rows[SelectedID1 - 1];
            // Name
            TxtbName1.Text = row[COL_NAME].ToString();
            // Fighting data
            if(Scrbrd != null) {
                Scrbrd.LblDevState1.Text = "Состояние: " + row[COL_DEVSTATE].ToString();
                Scrbrd.LblHitcnt1.Text = "Хиты: " + row[COL_HITS].ToString();
                Scrbrd.LblShotcnt1.Text = "Осталось выстрелов: " + row[COL_SHOTS].ToString();
                Scrbrd.LblTargetCnt1.Text = "Поражено мишеней: " + row[COL_TARGETS].ToString();
            }
            // === ID2 ===
            row = TableDevices.Rows[SelectedID2 - 1];
            // Name
            TxtbName2.Text = row[COL_NAME].ToString();
            // Fighting data
            if(Scrbrd != null) {
                Scrbrd.LblDevState2.Text = "Состояние: " + row[COL_DEVSTATE].ToString();
                Scrbrd.LblHitcnt2.Text = "Хиты: " + row[COL_HITS].ToString();
                Scrbrd.LblShotcnt2.Text = "Осталось выстрелов: " + row[COL_SHOTS].ToString();
                Scrbrd.LblTargetCnt2.Text = "Поражено мишеней: " + row[COL_TARGETS].ToString();
            }
        }

        // Show/hide Scorebrd
        private void ChbShowScoreboard_CheckedChanged(object sender, EventArgs e) {
            if(ChbShowScoreboard.Checked) {
                Scrbrd = new Scoreboard();
                // Copy text data to the board
                Scrbrd.LblCaption.Text = TxtbCaption.Text;
                Scrbrd.LblName1.Text = TxtbName1.Text;
                Scrbrd.LblName2.Text = TxtbName2.Text;
                RBtnState1_Click(null, null);
                DisplayDevDataOnScorebrd();
                Scrbrd.LblTime.Text = TxtbTimer.Text;
                ShowHideRight(ChbShowRight.Checked);
                Scrbrd.Show();
            }
            else {
                Scrbrd.Close();
                Scrbrd.Dispose();
                Scrbrd = null;
            }
        }

        // Show/hide right part of the board
        void ShowHideRight(bool MustShow) {
            Scrbrd.panelRight.Visible = MustShow;
            if(MustShow) Scrbrd.tableLayoutPanel1.ColumnCount = 2;
            else Scrbrd.tableLayoutPanel1.ColumnCount = 1;
        }


        private void ChbShowRight_CheckedChanged(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            ShowHideRight(ChbShowRight.Checked);
        }

        // On new info from transmitter
        private void TableDevices_RowChanged(object sender, System.Data.DataRowChangeEventArgs e) {
            int ID = int.Parse(e.Row[COL_ID].ToString());
            if(ID == SelectedID1 || ID == SelectedID2) DisplayDevDataOnScorebrd();
        }

        // State
        private void RBtnState1_Click(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            if(RBtnState1.Checked) Scrbrd.LblState.Text = TxtbState1.Text;
            else if(RBtnState2.Checked) Scrbrd.LblState.Text = TxtbState2.Text;
            else if(RBtnState3.Checked) Scrbrd.LblState.Text = TxtbState3.Text;
            else if(RBtnState4.Checked) Scrbrd.LblState.Text = TxtbState4.Text;
        }

        #region ==== Selection of attendee ====
        private void BtnSelect1_Click(object sender, EventArgs e) {
            if(DgvDevices.SelectedRows.Count == 0) {
                MessageBox.Show("В таблице ничего не выбрано. Выберите участника в таблице.", "Выбор участника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SelectedID1 = int.Parse(DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString());
            DisplayDevDataOnScorebrd();
        }

        private void BtnSelect2_Click(object sender, EventArgs e) {
            if(DgvDevices.SelectedRows.Count == 0) {
                MessageBox.Show("В таблице ничего не выбрано. Выберите участника в таблице.", "Выбор участника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SelectedID2 = int.Parse(DgvDevices.SelectedRows[0].Cells[COL_ID].Value.ToString());
            DisplayDevDataOnScorebrd();
        }
        #endregion

        #region ==== Time ====
        private int TimeS = 0;
        private void ChbTimerOn_CheckedChanged(object sender, EventArgs e) {
            TimerScore.Enabled = ChbTimerOn.Checked;
        }
        private void BtnTmrReset_Click(object sender, EventArgs e) {
            TimeS = 0;
            TxtbTimer.Text = "00:00";
        }
        private void TimerScore_Tick(object sender, EventArgs e) {
            TimeS++;
            int m = TimeS / 60;
            int s = TimeS % 60;
            TxtbTimer.Text = string.Format("{0:00}:{1:00}", m, s);
        }
        #endregion

        // Output to scorebrd in case of text change
        private void TxtbCaption_TextChanged(object sender, EventArgs e) {
            if(Scrbrd != null) Scrbrd.LblCaption.Text = TxtbCaption.Text;
        }

        // State change
        private void TxtbState1_TextChanged(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            if(RBtnState1.Checked) Scrbrd.LblState.Text = TxtbState1.Text;
        }
        private void TxtbState2_TextChanged(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            if(RBtnState2.Checked) Scrbrd.LblState.Text = TxtbState2.Text;
        }
        private void TxtbState3_TextChanged(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            if(RBtnState3.Checked) Scrbrd.LblState.Text = TxtbState3.Text;
        }
        private void TxtbState4_TextChanged(object sender, EventArgs e) {
            if(Scrbrd is null) return;
            if(RBtnState4.Checked) Scrbrd.LblState.Text = TxtbState4.Text;
        }

        private void TxtbName1_TextChanged(object sender, EventArgs e) {
            if(Scrbrd != null) Scrbrd.LblName1.Text = TxtbName1.Text;
        }
        private void TxtbName2_TextChanged(object sender, EventArgs e) {
            if(Scrbrd != null) Scrbrd.LblName2.Text = TxtbName2.Text;
        }



        private void TxtbTimer_TextChanged(object sender, EventArgs e) {
            if(Scrbrd != null) Scrbrd.LblTime.Text = TxtbTimer.Text;
        }

        #endregion

    }

    #region ========================= Classes =========================
    public struct DevInfo_t {
        public int ID;
        public string MsgType, Type, Group, HitCnt, ShotCnt, LastAttacker, State;
        public bool QIsEmpty;
    }

    public class MsgIDList_t {
        const int MaxCnt = 54;
        List<byte> IDs = new List<byte>(MaxCnt);

        public string GenerateAndPutNew() {
            byte NewID = 0;
            if(IDs.Count > 0) NewID = (byte)(IDs[IDs.Count - 1] + 1);
            IDs.Add(NewID);
            return NewID.ToString();
        }

        // Remove MsgID from the list 
        public void Remove(string SMsgID) {
            IDs.Remove(byte.Parse(SMsgID));
        }

        public void Flush() {
            IDs.Clear();
        }

        public bool IsEmpty { get { return (IDs.Count == 0); } }
    }
    
    public class Pipes_t {
        // Fields
        private bool IsOnline = false;
        SerialPort IPort;
        Timer ITimer;
        private int ICnt;
        MsgIDList_t[] MsgIDList;

        public void EnableTmr() {
            ITimer.Enabled = true;
        }

        // Events
        public delegate void EvtNewInfo(DevInfo_t Info);
        public event EvtNewInfo OnNewData;
        public delegate void EvtConnectDisconnect(bool IsConnected, string Description);
        public event EvtConnectDisconnect OnConnectDisconnect;

        #region ==== Inner use Send and reply ====
        private bool SendAndGetReply(string SCmd, out string SReply) {
            SReply = "";
            if(!IPort.IsOpen) return false;
            try {
                IPort.WriteLine(SCmd);
                SReply = IPort.ReadLine().Trim();
                return true;
            }
            catch(Exception ex) {
                try { IPort.Close(); }
                catch { }
                IsOnline = false;
                SReply = ex.Message;
            }
            return false;
        }

        public bool Ping(out string SReply) {
            if(SendAndGetReply("Ping", out SReply)) {
                return SReply.Equals("Ack 0", StringComparison.OrdinalIgnoreCase);
            }
            else return false;
        }

        public bool Flush(out string SReply) {
            if(SendAndGetReply("Flush", out SReply)) {
                return SReply.Equals("Ack 0", StringComparison.OrdinalIgnoreCase);
            }
            else return false;
        }
        #endregion

        #region ==== Port search and Timer related ====
        private void StartSearchOfTransmitter(string Reason = "") {
            OnConnectDisconnect?.Invoke(false, Reason);
            IsOnline = false;
            ITimer.Enabled = false;
            //MessageBox.Show("Поиск: " + Reason, "Why search", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // Flush all waiting messages
            foreach(MsgIDList_t MLst in MsgIDList) MLst.Flush();
            // Prepare timer
                ITimer.Interval = 720;
            ITimer.Tick -= ITimer_Tick_DataRequest;
            ITimer.Tick += ITimer_Tick_PortSearch;
            ITimer.Enabled = true;
        }

        private void StartDataRequesting() {
            ITimer.Interval = 207;
            ITimer.Tick -= ITimer_Tick_PortSearch;
            ITimer.Tick += ITimer_Tick_DataRequest;
        }

        private void ITimer_Tick_PortSearch(object sender, EventArgs e) {
            IsOnline = false;
            try {
                IPort.Close();
            }
            catch { }
            // Get port names
            string[] PortsNames = SerialPort.GetPortNames();
            if(PortsNames.Length > 0) {
                // Iterate discovered ports
                foreach(string PortName in PortsNames) {
                    IPort.PortName = PortName;
                    try {
                        IPort.Open();
                        if(Flush(out string SReply)) {    // Ping succeded, transmitter found (and flushed)
                            IsOnline = true;
                            OnConnectDisconnect?.Invoke(true, PortName);
                            StartDataRequesting();
                            return; 
                        }
                        else IPort.Close();
                    }
                    catch(Exception ex) {
                        OnConnectDisconnect?.Invoke(false, ex.Message);
                        IPort.Close();
                    }
                } // foreach
                // Silence answers our cries
                OnConnectDisconnect?.Invoke(false, "Ни на одном из портов передатчик не обнаружен");
            }
            else OnConnectDisconnect?.Invoke(false, "в системе нет ни одного последовательного порта");
        }

        private void ITimer_Tick_DataRequest(object sender, EventArgs e) {
            if(!GetInfo(out string SReply)) StartSearchOfTransmitter(SReply);
        }
        #endregion

        #region ==== Constructor / destructor ====
        public Pipes_t(int DevCnt) {
            ICnt = DevCnt;
            MsgIDList = new MsgIDList_t[DevCnt];
            for(int i = 0; i < DevCnt; i++) MsgIDList[i] = new MsgIDList_t();
            IPort = new SerialPort("COM1", 115200, Parity.None, 8, StopBits.One) {
                ReadTimeout = 630
            };
            ITimer = new Timer();
            StartSearchOfTransmitter();
        }

        ~Pipes_t() {
            ITimer.Enabled = false;
            try {
                IPort.Close();
                IPort.Dispose();
            }
            catch { }
        }
        #endregion

        #region ======= Sending and getting data ========
        public void SendParam(string DevID, int ParamID, string Data) {
            if(!IsOnline) return;
            int ID = int.Parse(DevID);
            string Cmd = "SetParam " + DevID + " " + MsgIDList[ID - 1].GenerateAndPutNew() + " " + ParamID.ToString() + " " + Data;
            IPort.WriteLine(Cmd);
        }

        public void SendNewGroup(string DevID, string GroupID) {
            SendParam(DevID, 20, GroupID);
        }
        public void SendHitCnt(string DevID, string HitCnt) {
            SendParam(DevID, 21, HitCnt);
        }
        public void SendShotCnt(string DevID, string ShotCnt) {
            SendParam(DevID, 22, ShotCnt);
        }
        public void SendState(string DevID, string State) {
            SendParam(DevID, 23, State);
        }

        private bool GetInfo(out string SReply) {
            try {
                IPort.WriteLine("GetInfo");
                // Get count of following data
                SReply = IPort.ReadLine().Trim();
                string[] Tokens = SReply.Split(new Char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if(!Tokens[0].Equals("Cnt", StringComparison.OrdinalIgnoreCase) || Tokens.Length != 2) return true;  // Something unexpected returned
                if(!int.TryParse(Tokens[1], out int Cnt)) return false; // Something unreadable
                // Read all info line by line
                for(int i = 0; i < Cnt; i++) {
                    SReply = IPort.ReadLine().Trim();
                    if(!ProcessInfoString(SReply)) return false;
                }
                return true;
            }
            catch(Exception ex) {
                try { IPort.Close(); }
                catch { }
                SReply = ex.Message;
            }
            return false;
        }

        private bool ProcessInfoString(string S) {
            string[] Tokens = S.Split(new Char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if(Tokens.Length != 9) return false;
            // Construct DevInfo
            DevInfo_t Info = new DevInfo_t() {
                ID = int.Parse(Tokens[0]),
                MsgType = Tokens[2],
                Type = Tokens[3], 
                Group = Tokens[4],
                HitCnt = Tokens[5],
                ShotCnt = Tokens[6],
                LastAttacker = Tokens[7],
                State = Tokens[8]
            };
            // Check if ID in range 
            if(Info.ID > ICnt) return true;
            // Process MsgType
            if(Info.MsgType.Equals("0") || Info.MsgType.Equals("1")) { // Data with OK or FAILURE msg info 
                // Remove MsgID from list 
                string SMsgID = Tokens[1];
                MsgIDList[Info.ID - 1].Remove(SMsgID);
            }
            Info.QIsEmpty = MsgIDList[Info.ID - 1].IsEmpty;
            OnNewData?.Invoke(Info);
            return true;
        }

        #endregion
    }
    #endregion
}
