﻿namespace Robotlon_sw {
    partial class Scoreboard {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scoreboard));
            this.LblTime = new System.Windows.Forms.Label();
            this.LblCaption = new System.Windows.Forms.Label();
            this.LblState = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LblTargetCnt1 = new System.Windows.Forms.Label();
            this.LblShotcnt1 = new System.Windows.Forms.Label();
            this.LblHitcnt1 = new System.Windows.Forms.Label();
            this.LblDevState1 = new System.Windows.Forms.Label();
            this.LblName1 = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.GrbRight = new System.Windows.Forms.GroupBox();
            this.LblTargetCnt2 = new System.Windows.Forms.Label();
            this.LblShotcnt2 = new System.Windows.Forms.Label();
            this.LblHitcnt2 = new System.Windows.Forms.Label();
            this.LblDevState2 = new System.Windows.Forms.Label();
            this.LblName2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.GrbRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblTime
            // 
            this.LblTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LblTime.Font = new System.Drawing.Font("Times New Roman", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblTime.Location = new System.Drawing.Point(4, 531);
            this.LblTime.Margin = new System.Windows.Forms.Padding(4);
            this.LblTime.Name = "LblTime";
            this.LblTime.Padding = new System.Windows.Forms.Padding(4);
            this.LblTime.Size = new System.Drawing.Size(1130, 86);
            this.LblTime.TabIndex = 1;
            this.LblTime.Text = "00:00";
            this.LblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblCaption
            // 
            this.LblCaption.BackColor = System.Drawing.Color.Lavender;
            this.LblCaption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblCaption.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblCaption.Font = new System.Drawing.Font("Times New Roman", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblCaption.Location = new System.Drawing.Point(4, 4);
            this.LblCaption.Margin = new System.Windows.Forms.Padding(4);
            this.LblCaption.Name = "LblCaption";
            this.LblCaption.Padding = new System.Windows.Forms.Padding(4);
            this.LblCaption.Size = new System.Drawing.Size(1130, 86);
            this.LblCaption.TabIndex = 2;
            this.LblCaption.Text = "Шапка";
            this.LblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblState
            // 
            this.LblState.BackColor = System.Drawing.Color.LavenderBlush;
            this.LblState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblState.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblState.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblState.Location = new System.Drawing.Point(4, 90);
            this.LblState.Margin = new System.Windows.Forms.Padding(4);
            this.LblState.Name = "LblState";
            this.LblState.Padding = new System.Windows.Forms.Padding(4);
            this.LblState.Size = new System.Drawing.Size(1130, 65);
            this.LblState.TabIndex = 3;
            this.LblState.Text = "Бой";
            this.LblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panelRight, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelLeft, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 155);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.57784F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.42216F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1130, 376);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.groupBox1);
            this.panelLeft.Controls.Add(this.LblName1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(3, 3);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(559, 370);
            this.panelLeft.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Ivory;
            this.groupBox1.Controls.Add(this.LblTargetCnt1);
            this.groupBox1.Controls.Add(this.LblShotcnt1);
            this.groupBox1.Controls.Add(this.LblHitcnt1);
            this.groupBox1.Controls.Add(this.LblDevState1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(559, 305);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // LblTargetCnt1
            // 
            this.LblTargetCnt1.AutoSize = true;
            this.LblTargetCnt1.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblTargetCnt1.Location = new System.Drawing.Point(30, 224);
            this.LblTargetCnt1.Name = "LblTargetCnt1";
            this.LblTargetCnt1.Size = new System.Drawing.Size(437, 49);
            this.LblTargetCnt1.TabIndex = 3;
            this.LblTargetCnt1.Text = "Поражено мишеней: 0";
            // 
            // LblShotcnt1
            // 
            this.LblShotcnt1.AutoSize = true;
            this.LblShotcnt1.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShotcnt1.Location = new System.Drawing.Point(28, 156);
            this.LblShotcnt1.Name = "LblShotcnt1";
            this.LblShotcnt1.Size = new System.Drawing.Size(462, 49);
            this.LblShotcnt1.TabIndex = 2;
            this.LblShotcnt1.Text = "Осталось выстрелов: 15";
            // 
            // LblHitcnt1
            // 
            this.LblHitcnt1.AutoSize = true;
            this.LblHitcnt1.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblHitcnt1.Location = new System.Drawing.Point(28, 93);
            this.LblHitcnt1.Name = "LblHitcnt1";
            this.LblHitcnt1.Size = new System.Drawing.Size(176, 49);
            this.LblHitcnt1.TabIndex = 1;
            this.LblHitcnt1.Text = "Хиты: 18";
            // 
            // LblDevState1
            // 
            this.LblDevState1.AutoSize = true;
            this.LblDevState1.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDevState1.Location = new System.Drawing.Point(28, 25);
            this.LblDevState1.Name = "LblDevState1";
            this.LblDevState1.Size = new System.Drawing.Size(423, 49);
            this.LblDevState1.TabIndex = 0;
            this.LblDevState1.Text = "Состояние: Исправен";
            // 
            // LblName1
            // 
            this.LblName1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LblName1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblName1.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblName1.Font = new System.Drawing.Font("Times New Roman", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblName1.Location = new System.Drawing.Point(0, 0);
            this.LblName1.Margin = new System.Windows.Forms.Padding(4);
            this.LblName1.Name = "LblName1";
            this.LblName1.Padding = new System.Windows.Forms.Padding(4);
            this.LblName1.Size = new System.Drawing.Size(559, 65);
            this.LblName1.TabIndex = 5;
            this.LblName1.Text = "Название 1";
            this.LblName1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.GrbRight);
            this.panelRight.Controls.Add(this.LblName2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(568, 3);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(559, 370);
            this.panelRight.TabIndex = 7;
            // 
            // GrbRight
            // 
            this.GrbRight.BackColor = System.Drawing.Color.Honeydew;
            this.GrbRight.Controls.Add(this.LblTargetCnt2);
            this.GrbRight.Controls.Add(this.LblShotcnt2);
            this.GrbRight.Controls.Add(this.LblHitcnt2);
            this.GrbRight.Controls.Add(this.LblDevState2);
            this.GrbRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrbRight.Location = new System.Drawing.Point(0, 65);
            this.GrbRight.Name = "GrbRight";
            this.GrbRight.Size = new System.Drawing.Size(559, 305);
            this.GrbRight.TabIndex = 8;
            this.GrbRight.TabStop = false;
            // 
            // LblTargetCnt2
            // 
            this.LblTargetCnt2.AutoSize = true;
            this.LblTargetCnt2.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblTargetCnt2.Location = new System.Drawing.Point(30, 224);
            this.LblTargetCnt2.Name = "LblTargetCnt2";
            this.LblTargetCnt2.Size = new System.Drawing.Size(437, 49);
            this.LblTargetCnt2.TabIndex = 3;
            this.LblTargetCnt2.Text = "Поражено мишеней: 0";
            // 
            // LblShotcnt2
            // 
            this.LblShotcnt2.AutoSize = true;
            this.LblShotcnt2.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShotcnt2.Location = new System.Drawing.Point(28, 156);
            this.LblShotcnt2.Name = "LblShotcnt2";
            this.LblShotcnt2.Size = new System.Drawing.Size(462, 49);
            this.LblShotcnt2.TabIndex = 2;
            this.LblShotcnt2.Text = "Осталось выстрелов: 15";
            // 
            // LblHitcnt2
            // 
            this.LblHitcnt2.AutoSize = true;
            this.LblHitcnt2.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblHitcnt2.Location = new System.Drawing.Point(28, 93);
            this.LblHitcnt2.Name = "LblHitcnt2";
            this.LblHitcnt2.Size = new System.Drawing.Size(176, 49);
            this.LblHitcnt2.TabIndex = 1;
            this.LblHitcnt2.Text = "Хиты: 18";
            // 
            // LblDevState2
            // 
            this.LblDevState2.AutoSize = true;
            this.LblDevState2.Font = new System.Drawing.Font("Roboto Slab", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDevState2.Location = new System.Drawing.Point(28, 25);
            this.LblDevState2.Name = "LblDevState2";
            this.LblDevState2.Size = new System.Drawing.Size(423, 49);
            this.LblDevState2.TabIndex = 0;
            this.LblDevState2.Text = "Состояние: Исправен";
            // 
            // LblName2
            // 
            this.LblName2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LblName2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LblName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.LblName2.Font = new System.Drawing.Font("Times New Roman", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LblName2.Location = new System.Drawing.Point(0, 0);
            this.LblName2.Margin = new System.Windows.Forms.Padding(4);
            this.LblName2.Name = "LblName2";
            this.LblName2.Padding = new System.Windows.Forms.Padding(4);
            this.LblName2.Size = new System.Drawing.Size(559, 65);
            this.LblName2.TabIndex = 7;
            this.LblName2.Text = "Название 2";
            this.LblName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Scoreboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 621);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.LblState);
            this.Controls.Add(this.LblCaption);
            this.Controls.Add(this.LblTime);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Scoreboard";
            this.Padding = new System.Windows.Forms.Padding(4);
            this.Text = "Robotlon";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.GrbRight.ResumeLayout(false);
            this.GrbRight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label LblTime;
        public System.Windows.Forms.Label LblCaption;
        public System.Windows.Forms.Label LblState;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Panel panelRight;
        public System.Windows.Forms.GroupBox GrbRight;
        public System.Windows.Forms.Label LblTargetCnt2;
        public System.Windows.Forms.Label LblShotcnt2;
        public System.Windows.Forms.Label LblHitcnt2;
        public System.Windows.Forms.Label LblDevState2;
        public System.Windows.Forms.Label LblName2;
        public System.Windows.Forms.Panel panelLeft;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label LblTargetCnt1;
        public System.Windows.Forms.Label LblShotcnt1;
        public System.Windows.Forms.Label LblHitcnt1;
        public System.Windows.Forms.Label LblDevState1;
        public System.Windows.Forms.Label LblName1;
    }
}